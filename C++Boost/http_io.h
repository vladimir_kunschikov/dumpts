#pragma once
#include "stream_handler.h"
#include <set>
#include <boost/asio.hpp>
using std::string;
using boost::asio::ip::tcp;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPCommon                                                                    *
 * base class for the HTTP IO stream handlers                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class HTTPCommon: public StreamHandler{
    public:
        HTTPCommon();
        ~HTTPCommon();
        void set_url(const string& url);
        int status() const;
    protected:
        tcp::iostream *stream;
        string url;
        int state;
        size_t content_length;
        tcp::iostream *create_http_connection(const string& url, const string& query);
        bool read_header(std::basic_iostream<char>& s, string& header);
        int http_status(const string& header);
        string get_tag(const string& header, const string& tag);
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPInput                                                                     *
 * HTTP Input 'GET' stream handler                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class HTTPInput: public HTTPCommon{
    StreamHandler* clone();
    void run();
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPOutput                                                                    *
 * HTTP Output 'Put' stream handler with MPEG TS packet filtering support        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class HTTPOutput: public HTTPCommon{
    StreamHandler* clone();
    void run();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MPEGTS PID filtering                                                          *
 * allowed_pids: set of the allowed to write pids : {0, 17, 256, etc}            *
 * All pids are allowed if allowed pids set is empty                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    std::set<int> allowed_pids;
    void set_allowed_pid(const string& pids);
    void filter(const string& data);
};
