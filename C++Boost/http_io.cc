#include "http_io.h"
#include "mpegts.h"
#include "buffer.h"
#include "tools.h"

#include <stdlib.h>
#include <iostream>
#include <boost/regex.hpp>
#define CRLF "\r\n"
using namespace std;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPCommon                                                                    *
 * base class for the HTTP IO stream handlers                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
HTTPCommon::HTTPCommon():stream(NULL), state(StreamHandler::OK), content_length(0)
{}

HTTPCommon::~HTTPCommon()
{
    delete stream;
}

void HTTPCommon::set_url(const string& url)
{
    this->url = url;
}

int HTTPCommon::status() const
{
    return state;
}

string HTTPCommon::get_tag(const string& header, const string& tag)
{
    string status;
    try{
        boost::regex pat(string(CRLF) + tag + ": *([^\\r]+)", boost::regex::icase);
        boost::smatch m;
        if (regex_search(header, m, pat))
            return m[1];
    }catch(boost::regex_error& e){
        cerr << "get_tag(" << tag << ") regex error #" << e.code() << endl << "Whole header: " << endl << header << endl;
    }

    return "";
}

bool HTTPCommon::read_header(std::basic_iostream<char>& s, string& header)
{
    while(s.good()){
        char c;
        s.read(&c, 1);
        header.append(&c, s.gcount());
        if(header.size() < 4 or c != '\n' or header.substr(header.size() - 4) != CRLF CRLF)
            continue;
        return true;
    }
    return false;
}

int HTTPCommon::http_status(const string& header)
{
    try{
        boost::regex pat("^[^ ]+ (\\d+) ", boost::regex::icase);
        boost::smatch m;
        if (regex_search(header, m, pat))
            return atoi(string(m[1]).c_str());
    }catch(boost::regex_error& e){
        cerr << "HTTP status regex error #" << e.code() << endl << "Whole header:" << endl << header;
    }
    return 0;
}

tcp::iostream *HTTPCommon::create_http_connection(const string& url, const string& query)
{
    map<string, string> tokens;
    parse_url(url, tokens);
    string host = tokens["host"];
    string port = tokens["port"];
    string page = tokens["page"];
    if(host.empty())
        host = "127.0.0.1";
    if(port.empty())
        port = "http";
    if(page.empty())
        port = "/";
    auto_ptr<tcp::iostream> connection(new tcp::iostream(host, port));
    *connection << query + " " + page + " HTTP/1.0" CRLF "Host: " + host + CRLF CRLF;
    string header;
    if(not read_header(*connection, header)){
        cerr << "failed to read header of the '" << url << "' query " << endl;
        return NULL;
    }

    switch(int status = http_status(header)){
        case 301:
        case 302:
        case 303:
            cout << "Relocation of  '" << url << "' to url from the 'location' header tag:" << endl << header << endl;
            return create_http_connection(get_tag(header, "location"), query);

        default:
            cerr << "HTTP status " << status << " in header:" << endl << header << endl;
            return NULL;

        case 200:
        case 206:
            content_length = atoi(get_tag(header, "Content-Length").c_str());
    }

    return connection.release();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPInput                                                                     *
 * HTTP Input 'GET' stream handler                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void HTTPInput::run()
{
    stream = create_http_connection(url, "GET");
    if(not stream){
        std::cerr << "[http input] Refused to handle '" << url << "'[" << string_error() << "]" << std::endl;
        state = StreamHandler::Error;
        return;
    }
 
    size_t file_length = 0;
    while(stream->good() and (not content_length or file_length < content_length)){
        char stream_buffer[TS_PACKET_SIZE];
        stream->read(stream_buffer, sizeof(stream_buffer));
        int status =  stream->gcount();
        if(status == -1){
            cerr << "failed to read " << url << ": " << string_error() << endl;
            break;
        }
        if(!status){
            cerr << "EOF while read from " << url << endl;
            break;
        }
 
        buffer->write_data_to_buffer(string(stream_buffer, status));
        file_length += status;
    }
    std::cout << "Has downloaded " << file_length << " bytes from the '" << url << "'." << std::endl;
    buffer->write_data_to_buffer(string());
}

StreamHandler* HTTPInput::clone()
{
    return new HTTPInput;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPOutput                                                                    *
 * HTTP Output 'Put' stream handler with MPEG TS packet filtering support        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void HTTPOutput::set_allowed_pid(const string& pids)
{
    const string digits = "0123456789";
    for(size_t pos = 0; pos != string::npos; pos = pids.find_first_of(digits, pos)){
        size_t start = pos;
        size_t end = pids.find_first_not_of(digits, pos);
        pos = end;
        allowed_pids.insert(atoi(pids.substr(start, end).c_str()));
    }
}

void HTTPOutput::run()
{
    map<string, string> tokens;
    parse_url(url, tokens);
    set_allowed_pid(tokens["pid"]);
    stream = create_http_connection(url, "PUT");
    if(not stream){
        std::cerr << "[http output] Refused to handle '" << url << "'[" << string_error() << "]" << std::endl;
        state = StreamHandler::Error;
        return;
    }
 
    size_t buffer_position = buffer->get_start_position();
    string data;

    while(buffer->read_data_from_buffer(data, buffer_position))
        if(allowed_pids.empty())
            *stream << data;
        else
            filter(data);
}

void HTTPOutput::filter(const string& data)
{
    for(size_t i = 0; i < data.size(); i += TS_PACKET_SIZE){
        if(data.size() - i < TS_PACKET_SIZE){
            stream->write(data.data() + i, data.size() - i);
            return;
        }

        const char* ts_packet = data.data() + i;
        if(ts_packet[0] == 0x47 and not allowed_pids.count(get_pid(ts_packet)))
            continue;

        stream->write(ts_packet, TS_PACKET_SIZE);
    }
}

StreamHandler* HTTPOutput::clone()
{
    return new HTTPOutput;
}
