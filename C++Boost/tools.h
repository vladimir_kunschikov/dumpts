/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Various useful routines used in many places                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <map>
#include <string>
using std::string;
using std::map;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * parse_url()                                                                   *
 * parses given url to the key->value tokens map                                 *
 * in: url                                                                       *
 * out: tokens                                                                   *
 * return: token count                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int parse_url(const string& url, map<string, string>& tokens);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * string_error()                                                                *
 * strerror (3) wrapper                                                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
string string_error();
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * uptime()                                                                      *
 * current system uptime() in mks                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
uint64_t uptime();
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * write_and_check()                                                             *
 * writes whole given buffer                                                     *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int write_and_check(int sd, const char* buffer, size_t size);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * poll_for_input()                                                              *
 * polls given descriptor for incoming data                                      *
 * in: 'sd' file descriptor handle to be polled                                  *
 * in: 'timeout' timeout of data arrival                                         *
 * ret: true if any data is available, false on any error or timeout             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
bool poll_for_input(int sd, int timeout);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * gettid                                                                        *
 *  thread LWP identification                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
uint64_t gettid();
