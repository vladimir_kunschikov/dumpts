#pragma once
#include "stream_handler.h"
#include <stdint.h>
#include <set>
#include <boost/asio.hpp>
using std::string;
using namespace boost::asio;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * UDPCommon class                                                               *
 * base class for the UDP Multicast IO stream handlers                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class UDPCommon: public StreamHandler{
    public:
        UDPCommon();
        ~UDPCommon();
        void set_url(const string& url);
        int status() const;
    protected:
        io_service service;
        ip::udp::socket *sd;
        uint16_t port;
        string host;
        string url;
        int state;
        ip::udp::socket *open_multicast_socket(const string& host, uint16_t port);
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MulticastInput class                                                          *
 * UDP Multicast Input Stream handler                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class MulticastInput: public UDPCommon{
    StreamHandler* clone();
    void run();
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MulticastOutput class                                                         *
 * UDP multicast streaming with MPEG TS packet filtering support                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class MulticastOutput: public UDPCommon{
    StreamHandler* clone();
    void run();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MPEGTS PID filtering                                                          *
 * allowed_pids: set of the allowed to write pids : {0, 17, 256, etc}            *
 * All pids are allowed if allowed pids set is empty                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    std::set<int> allowed_pids;
    void set_allowed_pid(const string& pids);
    void filter(const string& data);
    void send(const char* buffer, size_t size);
};
