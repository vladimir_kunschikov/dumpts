#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <poll.h>
#include <time.h>
#include <sys/syscall.h>

#include "tools.h"
#include <iostream>
#include <boost/regex.hpp>
#ifdef debug
bool verbosity = true;
#else
bool verbosity = false;
#endif

int parse_url(const string& url, map<string, string>& tokens)
{
    try{
        boost::regex pat("((.*)://)?([^/:]+)(:(\\d+))?([^?]+)?", boost::regex::icase);
        boost::smatch m;
        if (regex_search(url, m, pat)){
            if(verbosity)
                std::cout << "Parsed url: '" << url << "'" << std::endl;
            for(size_t i = 0; verbosity and i < m.size(); i++) std::cout << "m[" << i << "] = " << m[i] << std::endl;
            tokens["prefix"] = m[1];
            tokens["type"] = m[2];
            tokens["host"] = m[3];
            tokens["port"] = m[5];
            tokens["page"] = m[6];
            string& filename = tokens["file"];
            filename = m[6];
            if(filename.empty())
                filename = tokens["host"];
            size_t filename_start = filename.rfind("/");
            if(filename_start != string::npos) 
                filename = filename.substr(filename_start + 1);
            if(tokens["prefix"].empty())
                filename = url;
            else if(tokens["prefix"] == "file://")
                filename = url.substr(7);
            size_t pid_start = filename.find("{");
            if(pid_start != string::npos){
                size_t pid_end = filename.find("}");
                tokens["pid"] = filename.substr(pid_start + 1, pid_end - pid_start - 1);
                filename.erase(pid_start);
            }
            return tokens.size();
        }
    }catch(boost::regex_error& e){
        std::cerr << "url regex error #" << e.code() << " while processing url '" << url << "'" << std::endl;
    }
    if(verbosity)
        std::cout << "Failed to parse url '" << url << "'" << std::endl;
    return 0;
}

std::string string_error()
{
    char message[1024] = {0};
    strerror_r(errno, message, sizeof(message));
    return message;
}

uint64_t uptime()
{
    struct timespec tv;

    clock_gettime(CLOCK_MONOTONIC, &tv);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_nsec / 1000;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * write_and_check()                                                             *
 * writes whole given buffer                                                     *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int write_and_check(int sd, const char* buffer, size_t size)
{
    size_t already_sent = 0;
    do{
        ssize_t block_size = write(sd, buffer + already_sent, size - already_sent);
        if(block_size == -1){
            fprintf(stderr, "failed to write: %s\n", strerror(errno));
            return -1;
        }
        already_sent += block_size;
    }while(already_sent < size);
    return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * poll_for_input()                                                              *
 * polls given descriptor for incoming data                                      *
 * in: 'sd' file descriptor handle to be polled                                  *
 * in: 'timeout' timeout of data arrival                                         *
 * ret: true if any data is available, false on any error or timeout             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
bool poll_for_input(int sd, int timeout)
{
    int status = 0;
    struct pollfd pollfds[1];
    pollfds[0].fd = sd;
    pollfds[0].events = POLLIN;

    status = poll(pollfds, 1, 1000 * timeout);
    if(status < 0){
        if(errno == EINTR)
            return false;

        std::cerr << "failed to poll: " <<  string_error() << std::endl;
        return false;
    }

    if(pollfds[0].revents != POLLIN)
        return false;
    return status;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * gettid                                                                        *
 *  thread LWP identification                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
uint64_t gettid()
{
    return syscall(SYS_gettid);
}
