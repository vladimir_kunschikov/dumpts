#include "udp_io.h"
#include "mpegts.h"
#include "buffer.h"
#include "tools.h"

#include <stdlib.h>
#include <iostream>
using namespace std;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * UDPCommon class                                                               *
 * base class for the UDP Multicast IO stream handlers                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
UDPCommon::UDPCommon(): sd(NULL), port(1234), host("224.224.224.1"), state(StreamHandler::NotInited)
{}

UDPCommon::~UDPCommon()
{
    delete sd;
}

void UDPCommon::set_url(const string& url)
{
    this->url = url;

    map<string, string> tokens;
    parse_url(url, tokens);
    if(tokens.count("host"))
        host = tokens["host"];
    if(tokens.count("port"))
        port = atoi(tokens["port"].c_str());
    if(not port)
        port = 1234;
    state = StreamHandler::OK;
}

int UDPCommon::status() const
{
    return state;
}

ip::udp::socket *UDPCommon::open_multicast_socket(const string& host, uint16_t port)
{
    ip::udp::endpoint endpoint(ip::address::from_string("0.0.0.0"), port);
    ip::udp::socket* socket = new ip::udp::socket(service, endpoint);
    if(socket)
        socket->set_option(ip::multicast::join_group(ip::address::from_string(host)));
    else
        cerr << "Failed to create socket for the " << host << ":" << port << endl;
    return socket;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MulticastInput class                                                          *
 * UDP Multicast Input Stream handler                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void MulticastInput::run()
{
    sd = open_multicast_socket(host, port);
    if(not sd){
        std::cerr << "Refused to handle '" << url << "'[" << string_error() << "]" << std::endl;
        state = StreamHandler::Error;
        return;
    }
 
    size_t file_length = 0;
    while(true){
        ip::udp::endpoint sender;
        char stream_buffer[TS_PACKET_SIZE];
        int status = sd->receive_from(boost::asio::buffer(stream_buffer), sender);
        if(status == -1){
            cerr << "failed to read " << url << ": " << string_error() << endl;
            break;
        }
        if(!status){
            cerr << "EOF while read from " << url << endl;
            break;
        }
 
        buffer->write_data_to_buffer(string(stream_buffer, status));
        file_length += status;
    }
    std::cout << "Has downloaded " << file_length << " bytes from the '" << url << "'." << std::endl;
    buffer->write_data_to_buffer(string());
}

StreamHandler* MulticastInput::clone()
{
    return new MulticastInput;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MulticastOutput class                                                         *
 * UDP multicast streaming with MPEG TS packet filtering support                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void MulticastOutput::set_allowed_pid(const string& pids)
{
    const string digits = "0123456789";
    for(size_t pos = 0; pos != string::npos; pos = pids.find_first_of(digits, pos)){
        size_t start = pos;
        size_t end = pids.find_first_not_of(digits, pos);
        pos = end;
        allowed_pids.insert(atoi(pids.substr(start, end).c_str()));
    }
}

void MulticastOutput::run()
{
    map<string, string> tokens;
    parse_url(url, tokens);
    set_allowed_pid(tokens["pid"]);

    
    sd = new ip::udp::socket(service, ip::udp::endpoint());
    if(not sd){
        std::cerr << "Refused to handle '" << url << "'[" << string_error() << "]" << std::endl;
        state = StreamHandler::Error;
        return;
    }
 
    size_t buffer_position = buffer->get_start_position();
    string data;

    while(buffer->read_data_from_buffer(data, buffer_position))
        if(allowed_pids.empty())
            send(data.data(), data.size());
        else
            filter(data);
}

void MulticastOutput::filter(const string& data)
{
    for(size_t i = 0; i < data.size(); i+=TS_PACKET_SIZE){
        if(data.size() - i < TS_PACKET_SIZE){
            send(data.data() + i, data.size() - i);
            return;
        }

        const char* ts_packet = data.data() + i;
        if(ts_packet[0] == 0x47 and not allowed_pids.count(get_pid(ts_packet)))
            continue;

        send(ts_packet, TS_PACKET_SIZE);
    }
}

StreamHandler* MulticastOutput::clone()
{
    return new MulticastOutput;
}

void MulticastOutput::send(const char* data, size_t size)
{
    ip::udp::endpoint endpoint(ip::address::from_string(host), port);
    sd->send_to(boost::asio::buffer(data, size), endpoint);
}
