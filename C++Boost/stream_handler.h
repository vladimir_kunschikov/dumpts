/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * StreamHandler interface                                                       *
 * Stream handling module abstraction.                                           *
 * Derived classes must override clone(), status() and Thread::run() methods     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include "thread.h"
#include <string>
class StreamHandler:public Thread{
    public:
        StreamHandler();
        virtual StreamHandler* clone() = 0;
        virtual void set_url(const std::string& url) = 0;
        virtual int status() const = 0;
        void set_buffer(class Buffer* buffer);

        static const int OK;
        static const int Error;
        static const int NotInited;
    protected:
    Buffer* buffer;
};
