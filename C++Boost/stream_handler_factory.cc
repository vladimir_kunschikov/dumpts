#include "stream_handler_factory.h"
#include "stream_handler.h"
#include "tools.h"

void StreamHandlerFactory::register_input(const string& type, StreamHandler* handler)
{
    input_handlers[type] = handler;
}

void StreamHandlerFactory::register_output(const string& type, StreamHandler* handler)
{
    output_handlers[type] = handler;
}

StreamHandler* StreamHandlerFactory::create_input(const string& url)
{
    string url_type = get_url_type(url);
    if(not input_handlers.count(url_type))
        return NULL;

    StreamHandler* dh = input_handlers[url_type]->clone();
    dh->set_url(url);
    return dh;
}

StreamHandler* StreamHandlerFactory::create_output(const string& url)
{
    string url_type = get_url_type(url);
    if(not output_handlers.count(url_type))
        return NULL;

    StreamHandler* dh = output_handlers[url_type]->clone();
    dh->set_url(url);
    return dh;
}

string StreamHandlerFactory::get_url_type(const string& url)
{
    map<string, string> tokens;
    parse_url(url, tokens);
    string url_type = tokens["type"];
    if(url_type.empty()){
        bool is_file = url.find(":") == string::npos;
        url_type = is_file? "file" : "udp";
    }
    return url_type;
}

StreamHandlerFactory::~StreamHandlerFactory()
{
    for(map<string, StreamHandler*>::iterator i = input_handlers.begin(); i != input_handlers.end(); i++)
        delete i->second;

    for(map<string, StreamHandler*>::iterator i = output_handlers.begin(); i != output_handlers.end(); i++)
        delete i->second;
}
