#include <iostream>
#include <stdlib.h>
#include "file_io.h"
#include "tools.h"
#include "mpegts.h"
#include "buffer.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * FileIO                                                                        *
 * Base class for the file read write stream handlers                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
FileIO::FileIO():state(StreamHandler::NotInited)
{}

int FileIO::status() const
{
    return state;
}

void FileIO::set_url(const string& url)
{
    this->url = url;
    map<string, string> tokens;
    parse_url(url, tokens);
    if(tokens.count("file")){
        filename = tokens["file"];
        state = StreamHandler::OK;
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * FileInput                                                                     *
 * Input file stream handler                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void FileInput::set_url(const string& url)
{
    FileIO::set_url(url);
    if(state != StreamHandler::OK)
        return;

    f.open(filename.c_str(), std::ios_base::in);
    if(not f.is_open()){
        std::cerr << "[input] Failed to open '" << filename << "': " << string_error() << std::endl;
        state = StreamHandler::Error;
    }
}

void FileInput::run()
{
    if(state != StreamHandler::OK){
        std::cerr << "Refused to handle '" << url << "'[" << state << "]" << std::endl;
        return;
    }
 
    size_t file_length = 0;
    while(f.good()){
        char stream_buffer[TS_PACKET_SIZE*7];
        f.read(stream_buffer, sizeof(stream_buffer));
        buffer->write_data_to_buffer(string(stream_buffer, f.gcount()));
        file_length += f.gcount();
    }
    std::cout << "Successfuly downloaded " << file_length << " bytes from the '" << url << "'." << std::endl;
    buffer->write_data_to_buffer(string());
}

StreamHandler* FileInput::clone()
{
    return new FileInput;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * FileOutput                                                                    *
 * File write stream handler with optional mpeg ts filtering                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void FileOutput::set_url(const string& url)
{
    FileIO::set_url(url);
    if(state != StreamHandler::OK)
        return;

    f.open(filename.c_str(), std::ios_base::out);
    if(not f.is_open()){
        std::cerr << "[output] Failed to open '" << filename << "': " << string_error() << std::endl;
        state = StreamHandler::Error;
    }

    map<string, string> tokens;
    parse_url(url, tokens);
    if(tokens["pid"].empty())
        return;

    set_allowed_pid(tokens["pid"]);
}

void FileOutput::set_allowed_pid(const string& pids)
{
    const string digits = "0123456789";
    for(size_t pos = 0; pos != string::npos; pos = pids.find_first_of(digits, pos)){
        size_t start = pos;
        size_t end = pids.find_first_not_of(digits, pos);
        pos = end;
        allowed_pids.insert(atoi(pids.substr(start, end).c_str()));
    }
}

void FileOutput::run()
{
    if(state != StreamHandler::OK){
        std::cerr << "Refused to handle '" << url << "'[" << state << "]" << std::endl;
        return;
    }
    size_t buffer_position = buffer->get_start_position();
    string data;

    while(buffer->read_data_from_buffer(data, buffer_position))
        if(allowed_pids.empty())
            f.write(data.data(), data.size());
        else
            filter(data);
}

void FileOutput::filter(const string& data)
{
    for(size_t i = 0; i < data.size(); i += TS_PACKET_SIZE){
        if(data.size() - i < TS_PACKET_SIZE){
            f.write(data.data() + i, data.size() - i);
            return;
        }

        const char* ts_packet = data.data() + i;
        if(ts_packet[0] == 0x47 and not allowed_pids.count(get_pid(ts_packet)))
            continue;
        f.write(ts_packet, TS_PACKET_SIZE);
    }
}

StreamHandler* FileOutput::clone()
{
    return new FileOutput;
}
