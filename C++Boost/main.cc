#include "stream_handler_factory.h"
#include "file_io.h"
#include "http_io.h"
#include "udp_io.h"
#include "buffer.h"
#include "pipe.h"
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

void print_usage(const std::string& appname)
{
    std::cout << "Multicast stream dumper/streamer with MPEG TS PID packet filtering support.\n"
        "Usage:\n"
        "\t " << appname << " SOURCE [OUTPUT] [OUTPUT] [OUTPUT] ...\n"
        "Parameters:\n"
        "\tSOURCE mandatory multicast input stream specification in host:port form."
        " Default port is 1234.\n"
        "\t\t Source multicast stream specification example with both host and port specified:\n"
        "\t\t\t 224.1.3.4:1235\n"
        "\t\t Source with udp:// prefix:\n"
        "\t\t\t udp://224.2.3.4:1234\n"
        "\t\t Source with default port 1234 fallback:\n"
        "\t\t\t 224.1.2.3\n"
        "\t [OUTPUT]"
        " optional output file path with optional MPEG TS PID specification in path{PIDs} form. Default: 'dump.ts'.\n"
        "\t\t Output file specification sample which dumps whole multicast stream to the /media/dump.ts:\n"
        "\t\t\t /media/stream.ts\n"
        "\t\t Output which dumps all 0 or 13 MPEG TS PID packets to the /media/pat_sdt.ts:\n"
        "\t\t\t '/media/pat_sdt.ts{0,13}'\n";
}

bool parse_command_line(int argc, char **argv, po::variables_map& vm)
{
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", " Prints help")
        ("url,u", po::value<vector<string> >(),  " Sets source url")
        ("output_bitrate,o", po::value<int>(), "Set output bitrate")
        ("input_bitrate,i", po::value<int>(), "Set intput bitrate");

    po::positional_options_description p;
    p.add("url", -1);
    try{
        po::store(po::command_line_parser(argc, argv).  options(desc).positional(p).run(), vm);
    }catch(po::error& e){
        std::cerr << e.what() << std::endl;
        return false;
    }
    po::notify(vm); 
    return true;
}

int main(int argc, char **argv)
{
    po::variables_map vm;
    if(not parse_command_line(argc, argv, vm)){
        print_usage(argv[0]);
        return 1;
    }
    vector<string> urls;
    if(vm.count("url"))
        urls = vm["url"].as<vector<string> >();

    if(urls.empty() or vm.count("help")){
        print_usage(argv[0]);
        return 0;
    }
    int input_bitrate = 0, output_bitrate = 0;
    if(vm.count("input_bitrate"))
        input_bitrate = vm["input_bitrate"].as<int>();
    if(vm.count("output_bitrate"))
        output_bitrate = vm["output_bitrate"].as<int>();

    StreamHandlerFactory factory;
    factory.register_output("file", new FileOutput);
    factory.register_output("http", new HTTPOutput);
    factory.register_output("udp", new MulticastOutput);
    factory.register_input("file", new FileInput);
    factory.register_input("http", new HTTPInput);
    factory.register_input("udp", new MulticastInput);

    Pipe pipe;
    pipe.set_buffer(new Buffer(input_bitrate, output_bitrate));
    pipe.add_producer(factory.create_input(urls[0]));

    if(urls.size() == 1){
        std::cout << "No output destination were specified. Fallback to the default 'dump.ts'." << std::endl;
        pipe.add_consumer(factory.create_output("dump.ts"));
    }

    for(size_t i = 1; i < urls.size(); i++)
        pipe.add_consumer(factory.create_output(urls[i]));

    return pipe.run();
}
