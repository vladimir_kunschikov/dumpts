#include "thread.h"
#include "tools.h"
#include  <iostream>

Thread::Thread():thread(NULL),started(false){}
Thread::~Thread()
{
    if(started and thread)
        thread->join();
    delete thread;
}

void Thread::start()
{
    thread = new boost::thread(boost::bind(&Thread::run, this));
    started = true;
}

void Thread::join()
{
    if(not started){
        std::cerr << "thread wasn't started at all" << std::endl;
        return;
    }
    if(thread and started)
        thread->join();
    started = false;
}
