/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * StreamHandlerFactory class                                                    *
 * Stores stream handler prototypes in register_*() method                       *
 * Clones stream handler prototypes in create_*() method                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <map>
#include <string>
using std::string;
using std::map;
class StreamHandler;
class StreamHandlerFactory{
    map <string, StreamHandler*> input_handlers;
    map <string, StreamHandler*> output_handlers;
    string default_input_url_type;
    string default_output_put_url_type;
    string get_url_type(const string& url);
    public:
        void register_input(const string& tag, StreamHandler* handler);
        void register_output(const string& tag, StreamHandler* handler);
        StreamHandler* create_input(const string& url);
        StreamHandler* create_output(const string& url);
        ~StreamHandlerFactory();
};
