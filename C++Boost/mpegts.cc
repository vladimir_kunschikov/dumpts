#include <stdint.h>
uint16_t get_pid(const char* ts_packet)
{
    return ((ts_packet[1] & 0x1f) << 8) | ts_packet[2];
}
