#include "pipe.h"
#include "buffer.h"
#include "stream_handler.h"
#include  <iostream>

Pipe::~Pipe()
{
    for(size_t i = 0; i < producers.size(); i++)
        delete producers[i];
    for(size_t i = 0; i < consumers.size(); i++)
        delete consumers[i];
    delete buffer;
}

void Pipe::add_producer(StreamHandler* source)
{
    producers.push_back(source);
}

void Pipe::add_consumer(StreamHandler* sink)
{
    consumers.push_back(sink);
}

int Pipe::run()
{
    if(producers.empty()){
        std::cerr << "No input given." << std::endl;
        return __LINE__;
    }


    if(not initialized(producers)){
        std::cerr << "failed to initialize any input" << std::endl;
        return __LINE__;
    }

    if(consumers.empty()){
        std::cerr << "No output given." << std::endl;
        return __LINE__;
    }

    if(not initialized(consumers)){
        std::cerr << "failed to initialize any output" << std::endl;
        return __LINE__;
    }

    start(producers);
    start(consumers);
    join(producers);
    join(consumers);
    return 0;
}

void Pipe::start(const vector<StreamHandler*>& handlers)
{
    for(size_t i = 0; i < handlers.size(); i++)
        if(StreamHandler* handler = handlers[i]){
            if(handler->status() != StreamHandler::OK)
                continue;
            handler->set_buffer(buffer);
            handler->start();
        }
}

void Pipe::join(const vector<StreamHandler*>& handlers)
{
    for(size_t i = 0; i < handlers.size(); i++)
        if(StreamHandler* handler = handlers[i]){
            if(handler->status() != StreamHandler::OK)
                continue;
            handler->join();
        }
}

size_t Pipe::initialized(const vector<StreamHandler*>& handlers)
{
    size_t count = 0;
    for(size_t i = 0; i < handlers.size(); i++)
        if(handlers[i] and handlers[i]->status() == StreamHandler::OK)
            count++;
    return count;
}

void Pipe::set_buffer(Buffer* b)
{
    buffer = b;
}
