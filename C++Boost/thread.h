/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Thread class                                                                  *
 * boost::thread wrapper                                                         *
 * Derived classes must override run() method                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <boost/thread/thread.hpp>
#pragma once
class Thread{
    public:
        Thread();
        virtual ~Thread();
        void start();
        void join();
        virtual void run() = 0;
    private:
        boost::thread *thread;
        bool started;

};
