# README #
Multicast/unicast dumper is intended  to retranslate and write MPEG2TS streams from network sources such as udp/http to file dumps and vice versa. Stream can be filtered, filtering is done by the pid number criteria. Multiple simultaneous source/destinations are supported. Tool can be used as multicast streamer with fixed bitrate.
Tool doesn't make any assumptions about stream. It means that in absence of pid specifications it is able to retransmit and save any stream/url without any data loss/corruption.  
 
### Usage ###
```
#!sh
dumper udp://224.1.1.1:1234 whole_stream.ts pat_table.ts{0} sdt_table.ts{17} video_254.ts{254} audio_stream_255.ts{255}  audio_stream_256.ts{256} 'video_and_audio_256.ts{254,256}' whole_stream_copy.ts udp://224.1.1.2:1234 'udp://222.222.222.2:1234{0,17}'
```
  Command above will dump multicast stream from 224.1.1.1:1234 source to the following files:

1.   whole_stream.ts - exact copy of the stream
1.   pat_table.ts  - Program Association Table stream definitions: packets with PID #0
1.   sdt_table.ts - Service description table dump: packets with pid #17
1.   video_254.ts - packets with pid 254.
1.   audio_stream_255.ts - packet with pid 255
1.   audio_stream_256.ts - packet with pid 256
1.   video_and_audio_256.ts packets with pids 254 and 256
1.   whole_stream_copy.ts another copy of the stream, same as whole_stream.ts, just for example
1.   224.1.1.2:1234 stream retranslation to another address
1.   222.222.222.2:1234 stream with 0 and 17 packets - PAT and SDT tables
```
#!sh
dumper video_and_audio.ts udp://224.1.1.1:1234 udp://224.1.1.1:1233{254}
```
  Command above will stream multicast 224.1.1.1:1234 from the video_and_audio.ts dump. Streaming to the :1233 port will be done only for the packets with 254 PID

```
#!sh
dumper http://www.example.net/video.ts video.ts udp://224.1.1.1:1234 udp://224.1.1.1:1233{254}
```
The same streaming as in  previous example but with source from the http file (not HLS).
```
#!sh
dumper udp://224.1.1.1:1234 udp://224.1.1.1:2540{254} 'udp://224.1.1.1:2556{255,256}'
```
Live runtime retransmition of the stream to the separate audio/video streams. Real pid numbers should be read from PMT. PMT with pid numbers can be parsed by ffmpeg/vlc/etc.
**Several pid specification in {0,1,2,3} format should be escaped by quotes to prevent shell expansion.**

 
* Tool doesn't use any configuration file.


### C C++ Boost ###
*  Three version of utility were written: on **C**, on pure **C++** and **C++ with Boost** library usage.

C/C++/Boost versions are in the corresponding folders. Application functionality doesn't depend on used language.

* Version 0.1 initial commit.

### How do I get set up? ###
* Summary of build process

Choose version to build, cd C or cd C++ or cd C++Boost and execute
```
#!sh
make
```

### Dependencies ###

* C: **libc**
* C++: **libstdc++**
* C++ Boost: **boost_system + boost_thread + boost_regex + boost_program_options 
**

### How to run tests ###

Just execute 'make test' in corresponding source folder. C++ depends on CPPUnit, C++Boost and C editions depend on Boost test system. Yes, C is using Boost, but for testing purposes only.

### Deployment instructions ###

use with care.
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Vladimir Kunschikov kunschikov@gmail.com http://stackoverflow.com/users/2277408/vladimir-kunschikov