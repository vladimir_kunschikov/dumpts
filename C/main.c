#include "source.h"
#include "destination.h"
#include "buffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void print_usage(const char* appname)
{
    printf(
            "Multicast stream dumper/streamer with MPEG TS PID packet filtering support.\n"
            "Usage:\n"
            "\t %s SOURCE [OUTPUT] [OUTPUT] [OUTPUT] ...\n"
            "Parameters:\n"
            "\tSOURCE mandatory multicast input stream specification in host:port form."
            " Default port is 1234.\n"
            "\t\t Source multicast stream specification example with both host and port specified:\n"
            "\t\t\t udp://224.1.3.4:1235\n"
            "\t\t Source with default port 1234 fallback:\n"
            "\t\t\t 224.1.2.3\n"
            "\t [OUTPUT]"
            " optional output file path with optional MPEG TS PID specification in path{PIDs} form. Default: 'dump.ts'.\n"
            "\t\t Output file specification sample which dumps whole multicast stream to the /media/dump.ts:\n"
            "\t\t\t /media/stream.ts\n"
            "\t\t Output which dumps all 0 or 13 MPEG TS PID packets to the /media/pat_sdt.ts:\n"
            "\t\t\t /media/pat_sdt.ts{0,13}\n",
            appname);
}


int main(int argc, char **argv)
{
    int opt, read_bitrate = 0, write_bitrate = 0;
    while((opt = getopt(argc, argv, "i:o:")) != -1){
        switch (opt) {
            case 'i':
                read_bitrate = atoi(optarg);
                break;
            case 'o':
                write_bitrate = atoi(optarg);
                break;
            default:
                print_usage(argv[0]);
                return -1;
        }
    }

    if(optind >= argc){
        print_usage(argv[0]);
        return 0;
    }
 
    const char* source_url = argv[optind];
    int source_fd = init_source(source_url);
    if(source_fd == -1){
        fprintf(stderr, "failed to initialize input '%s'\n", source_url);
        return __LINE__;
    }
    printf("source: %s [%d]\n", source_url, source_fd);

    destination_t destinations[argc + 1];
    size_t destination_count = init_destination(optind + 1 < argc ? (const char**)argv + optind + 1 : (const char * []) {"dump.ts", 0}, destinations);
    if(destination_count == 0){
        fprintf(stderr, "failed to initialize any output\n");
        close(source_fd);
        return __LINE__;
    }

    int ret = read_and_write(source_fd, destinations, destination_count, read_bitrate, write_bitrate);

    close(source_fd);
    int i;
    for(i = 0; i < destination_count; i++){
        if(destinations[i].pids)
            free(destinations[i].pids); 
        if(destinations[i].host)
            free(destinations[i].host); 
        close(destinations[i].fd);
    }

    return ret;
}
