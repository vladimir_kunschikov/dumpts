/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Toolkit: shared source routines                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <unistd.h>
#include <pthread.h>
#include "destination.h"
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * print_error()                                                                 *
 * perror() with more info                                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void print_error(const char* action, const char* filename);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * poll_and_read()                                                               *
 * wait for data, read it into the given buffer                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int poll_and_read(int sd, int timeout, char* buffer, size_t size);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * pthread_* wrappers                                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int lock(pthread_mutex_t * mutex);
int unlock(pthread_mutex_t * mutex);
int wait(pthread_cond_t *cond, pthread_mutex_t* mutex);
int wake(pthread_cond_t *cond);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * uptime()                                                                      *
 * current system uptime() in mks                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
uint64_t uptime();
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * parse_url()                                                                   *
 * parses url to the host port filename triple                                   *
 * in: 'url' in http://example.org:888/a/b/c/d/file.jpg form                     *
 * out: 'host': example.org in the url just above this line                      *
 * out: 'page': page url part                                                    *
 * out: 'port': 80 if not specified                                              *
 * out: 'filename': file.jpg                                                     *
 * ret: 0 on success non zero on error                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int parse_url(const char* url, char* proto, char* host, uint16_t *port, char* page, char* filename, char *pids);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * write_and_check()                                                             *
 * writes whole given buffer                                                     *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int write_and_check(int sd, const char* buffer, size_t size);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * sendto                                                                        *
 * send_to() wrapper with actually transmitted size control                      *
 * in: 'data, size' outgoing data                                                *
 * in: 'host, port' destination                                                  *
 * ret: zero if no errors, errno in other case                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int send_to(int sd, const char* data, size_t size, const char* host, uint16_t port);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * retransmit()                                                                  *
 * wrapper over write_and_check() and sendto()                                   *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * in: 'dest' optional data destination for the send_to() transmition case       *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int retransmit(int sd, const char* data, size_t size, const destination_t* destination);
