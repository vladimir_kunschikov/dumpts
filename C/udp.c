#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>

#include "tools.h"

int open_multicast_socket(const char* address, int port)
{
    int sd = -1;
    struct ip_mreq mreq;

    /* create what looks like an ordinary UDP socket */
    if ((sd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        print_error("create", "socket");
        return sd;
    }

    int yes = 1;            
    /* allow multiple sockets to use the same PORT number */
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0)
        print_error("setsockopt", "SO_REUSEADDR");

    /* use setsockopt() to request that the kernel join a multicast group */
    mreq.imr_multiaddr.s_addr = inet_addr(address);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
        print_error("setsockopt", "IP_ADD_MEMBERSHIP");
        close(sd);
        return -1;
    }

    struct sockaddr_in addr;
    /* set up destination address */
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = mreq.imr_multiaddr.s_addr;
    addr.sin_port = htons(port);

    /* bind to receive address */
    if (bind(sd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        print_error("bind", "input");
        close(sd);
        return -1;
    }

    return sd;
}
