#include "tools.h"
#include "http.h"
#include "file.h"
#include "udp.h"

#include <string.h>
#include <stdio.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * init_source()                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int init_source(const char* url)
{
    char proto[strlen(url) + 1];
    char host[strlen(url) + 1];
    uint16_t port;
    if(parse_url(url, proto, host, &port, NULL, NULL, NULL) == -1){
        fprintf(stderr, "failed to parse '%s' url\n", url);
        return -1;
    }

    if(!strcasecmp(proto, "HTTP"))
        return http_input(url);
    else if(!strcasecmp(proto, "udp"))
        return open_multicast_socket(host, port);
    else //if(!strcasecmp(proto, "file"))
        return file_open_for_read(url);
     return -1;
}
