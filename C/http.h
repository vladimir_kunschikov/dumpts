#pragma once
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_input()                                                                  *
 * Establishes tcp connection, sends GET, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: zero on success -1 on error                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_input(const char*);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_output()                                                                 *
 * Establishes tcp connection, sends PUT, analizes server reply                  *
 * in: 'url' data url                                                            *
 * ret: zero on success -1 on error                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_output(const char*);
