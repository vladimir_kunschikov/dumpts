#define BOOST_TEST_MODULE test_url
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <map>
using std::map;
using std::string;
bool debug = false;
extern "C" int parse_url(const char* url, char* proto, char* host, uint16_t *port, char* page, char* filename, char *pids);
void _parse_url(const string& url, map<string, string>& tokens)
{
    char proto[1024] = {0}, host[1024] = {0}, page[1024] = {0}, filename[1024] = {0}, pids[1024] = {0};
    uint16_t port = 0;
    int ret = parse_url(url.c_str(), proto, host, &port, page, filename, pids);
    tokens["type"] = proto;
    tokens["host"] = host;
    tokens["page"] = page;
    char sport[5] = {0};
    if(port)
        snprintf(sport, sizeof(sport), "%d", port);
    tokens["port"] = sport;
    tokens["file"] = filename;
    tokens["pid"] = pids;
}
#define parse_url _parse_url

BOOST_AUTO_TEST_SUITE(test_url)
BOOST_AUTO_TEST_CASE(test_url1)
{
    map<string, string> tokens;
    string url = "http://www.ru";
    parse_url(url, tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "http");
    tokens.clear();
    
    url = "http://www.ru/1.jpg";
    parse_url(url, tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "http");
    BOOST_CHECK_EQUAL(tokens["file"], "1.jpg");

}

BOOST_AUTO_TEST_CASE(test_url2)
{
    map<string, string> tokens;
    string url = "1.txt";
    parse_url(url, tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "1.txt");
    tokens.clear();
    
    url = "file://2.txt";
    parse_url(url, tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "file");
    BOOST_CHECK_EQUAL(tokens["file"], "2.txt");
}

BOOST_AUTO_TEST_CASE(test_url3)
{
    map<string, string> tokens;
    parse_url("udp://128.1.1.1:abcd", tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "udp");
    BOOST_CHECK_EQUAL(tokens["host"], "128.1.1.1");
    BOOST_CHECK_EQUAL(tokens["port"], "");
}

BOOST_AUTO_TEST_CASE(test_url4)
{
    map<string, string> tokens;
    parse_url("udp://128.1.1.1:1234", tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "udp");
    BOOST_CHECK_EQUAL(tokens["host"], "128.1.1.1");
    BOOST_CHECK_EQUAL(tokens["port"], "1234");
}

BOOST_AUTO_TEST_CASE(test_url5)
{
    map<string, string> tokens;
    parse_url("http://host.ru:7777/main/file.jpg?param=value&param2=value2", tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "http");
    BOOST_CHECK_EQUAL(tokens["host"], "host.ru");
    BOOST_CHECK_EQUAL(tokens["port"], "7777");
    BOOST_CHECK_EQUAL(tokens["file"], "file.jpg");
    BOOST_CHECK_EQUAL(tokens["page"], "/main/file.jpg");
}

BOOST_AUTO_TEST_CASE(test_url6)
{
    map<string, string> tokens;
    parse_url("http://host.ru:7777/file.jpg{12,13,14}?param=value&param2=value2", tokens);
    BOOST_CHECK_EQUAL(tokens["type"], "http");
    BOOST_CHECK_EQUAL(tokens["host"], "host.ru");
    BOOST_CHECK_EQUAL(tokens["port"], "7777");
    BOOST_CHECK_EQUAL(tokens["page"], "/file.jpg{12,13,14}");
    BOOST_CHECK_EQUAL(tokens["file"], "file.jpg");
    BOOST_CHECK_EQUAL(tokens["pid"], "12,13,14");
}

BOOST_AUTO_TEST_CASE(test_url7)
{
    map<string, string> tokens;
    parse_url("../file.txt", tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "../file.txt");
}

BOOST_AUTO_TEST_CASE(test_url8)
{
    map<string, string> tokens;
    parse_url("../../file.txt{3,2,1}", tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "../../file.txt");
    BOOST_CHECK_EQUAL(tokens["pid"], "3,2,1");
}

BOOST_AUTO_TEST_CASE(test_url9)
{
    map<string, string> tokens;
    parse_url("http://localhost/file.txt", tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "file.txt");
    BOOST_CHECK_EQUAL(tokens["host"], "localhost");
}

BOOST_AUTO_TEST_CASE(test_url10)
{
    map<string, string> tokens;
    parse_url("http://localhost/file.txt{3,2,1}", tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "file.txt");
    BOOST_CHECK_EQUAL(tokens["pid"], "3,2,1");
    BOOST_CHECK_EQUAL(tokens["host"], "localhost");
}

BOOST_AUTO_TEST_CASE(test_url11)
{
    map<string, string> tokens;
    parse_url("file{3,2,1}", tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "file");
    BOOST_CHECK_EQUAL(tokens["pid"], "3,2,1");
}

BOOST_AUTO_TEST_CASE(test_url12)
{
    map<string, string> tokens;
    parse_url("file", tokens);
    BOOST_CHECK_EQUAL(tokens["file"], "file");
}
BOOST_AUTO_TEST_SUITE_END()
