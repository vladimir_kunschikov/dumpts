/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * udp multicast socket creation with multicast group membership                 *
 * in:  'group' 224.224.224                                                      *
 * in:  'port' 1234                                                              *
 * ret: file descriptor or -1                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
int open_multicast_socket(const char* group, int port);
