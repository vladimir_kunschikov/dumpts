/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * init_destination()                                                            *
 * in: 'urls' NULL-terminated array of urls                                      *
 * out: 'destination_t' array of the created destination specifications          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once 
#include <stdint.h>
typedef struct {
    int fd;
    int* pids; //allowed mpeg ts packet PID array
    char* host; //destination for the multicast streams
    uint16_t port; //destination port
} destination_t;
int init_destination(const char** urls, destination_t* fds);
