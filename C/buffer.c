#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include "tools.h"
#include "mpegts.h"
#include "destination.h"

#define MAX_BUFFER_SIZE 255
#define DATA_UNIT_SIZE TS_PACKET_SIZE*10
pthread_mutex_t  mutex;
pthread_cond_t  condition;

typedef struct {
    uint16_t size;
    char buffer[DATA_UNIT_SIZE];
} NetworkDataUnit;

int input_bitrate = -1, output_bitrate = -1;
NetworkDataUnit buffer[MAX_BUFFER_SIZE];
int buffer_thread_count[MAX_BUFFER_SIZE] = {0};
int read_position = 0;

void wait_for_writers()
{
    lock(&mutex);
    read_position = read_position + 1 < MAX_BUFFER_SIZE? read_position + 1 : 0;
    
    while(buffer_thread_count[read_position]){
        //ongoing processing of the buffer[read_position] is being made 
        wake(&condition); //wake if somebody was sleeping
        wait(&condition, &mutex); //unlock and wait
    }

    wake(&condition);
    unlock(&mutex);
}

void limit_bitrate(int bitrate, int* bitrate_block_count, int64_t *bitrate_timestamp)
{
    if(bitrate <= 0)
        return;
    if(uptime() < *bitrate_timestamp + 10)
        return;
    while(*bitrate_block_count > bitrate * (uptime() - *bitrate_timestamp)/1024)
        usleep(10);
    *bitrate_block_count = 0;
    *bitrate_timestamp = uptime(0);
}

void * reader(void * v)
{
    int sd = *(int *)v;
    time_t last_data_timestamp = time(0);
    time_t lifestart = time(0);
    int64_t bitrate_timestamp = uptime();
    long long int  total_read_count = 0;
    int bitrate_read_count = 0;
    while(last_data_timestamp > time(0) - 2){
        NetworkDataUnit* unit = buffer + read_position;
        int timeout = 1000;
        int size = poll_and_read(sd, timeout, unit->buffer, DATA_UNIT_SIZE);
        if(size < 0)
            break;
        if(!size)
            continue;

        unit->size = size;
        bitrate_read_count += size;
        total_read_count += size;

        limit_bitrate(input_bitrate, &bitrate_read_count, &bitrate_timestamp);
        wait_for_writers();
        last_data_timestamp = time(0);
    }
    int spent = time(0) - lifestart + 1;
    printf("Has read %lld bytes == %f kb == %f mb in %d seconds. Bitrate: %f kb/sec\n",
            total_read_count, total_read_count/1024.0, total_read_count/1024/1024.0, spent, total_read_count/1024.0/spent);
    buffer_thread_count[read_position] = -1;
    return NULL;
}


int update_write_position(int* p_thread_write_position)
{
    int thread_write_position = *p_thread_write_position;
    lock(&mutex);

    thread_write_position = thread_write_position + 1 < MAX_BUFFER_SIZE? thread_write_position + 1 : 0;

    if(thread_write_position == MAX_BUFFER_SIZE)
        thread_write_position = 0;

    if(buffer_thread_count[thread_write_position] == -1){
        unlock(&mutex);
        return 0;
    }
    while(read_position == thread_write_position && buffer_thread_count[read_position] != -1){
        wake(&condition);
        wait(&condition, &mutex);
    }

    if(buffer_thread_count[thread_write_position] == -1){
        unlock(&mutex);
        return 0;
    }

    buffer_thread_count[thread_write_position]++;
    buffer_thread_count[*p_thread_write_position]--;
    unlock(&mutex);

    *p_thread_write_position = thread_write_position;
    return 1;
}

void packet_filter(int sd, char* data, size_t size, int* pids, destination_t* dest)
{
    size_t i,j;
    for(i = 0; i < size; i += TS_PACKET_SIZE){
        if(size - i < TS_PACKET_SIZE){
            retransmit(sd, data + i, size - i, dest);
            return;
        }

        if(data[i] != 0x47) 
            continue;
        int pid = get_pid(data + i);
        for(j = 0; pids[j] != -1; j++)
        if(pid == pids[j]){
            retransmit(sd, data + i, TS_PACKET_SIZE, dest);
            continue;
        }
    }
}

void * writer(void * v)
{
    destination_t *destination = (destination_t *)v;
    int sd = destination->fd;
    int thread_write_position = MAX_BUFFER_SIZE - 1;
    long long int total_write_count = 0;
    int bitrate_write_count = 0;
    int64_t bitrate_timestamp = uptime();
    time_t lifestart = time(0);
    int* pids = destination->pids;
    while(update_write_position(&thread_write_position)){
        limit_bitrate(output_bitrate, &bitrate_write_count, &bitrate_timestamp);
        NetworkDataUnit* unit = buffer + thread_write_position;
        if(pids)
            packet_filter(sd, unit->buffer, unit->size, pids, destination);
        else
            retransmit(sd, unit->buffer, unit->size, destination);
        total_write_count += unit->size;
        bitrate_write_count += unit->size;
    }
    int spent = time(0) - lifestart + 1;
    printf("Has written %lld bytes == %f kb == %f mb in %d seconds. Bitrate: %f kb/sec\n",
            total_write_count, total_write_count/1024.0, total_write_count/1024/1024.0, spent, total_write_count/1024.0/spent);

    return NULL;
}

int read_and_write(int source, destination_t* dest, int dest_count, int read_bitrate, int write_bitrate)
{
    pthread_t source_thread;
    pthread_t output_threads[dest_count];
    input_bitrate = read_bitrate;
    output_bitrate = write_bitrate;
    printf("input bitrate limit %d output bitrate limit %d\n", input_bitrate, output_bitrate);
    //start all of the output writers
    buffer_thread_count[MAX_BUFFER_SIZE - 1] = dest_count;
    int i = 0;
    for(i = 0; i < dest_count; i++)
        pthread_create(output_threads + i, NULL, writer, dest + i);
    //start input stream reader
    pthread_create(&source_thread, NULL, reader, &source);
    //join input stream reader
    pthread_join(source_thread, NULL);
    printf("joined source thread\n");
    wake(&condition);
    //join output stream writers
    for(i = 0; i < dest_count; i++)
        pthread_join(output_threads[i], NULL);
    printf("joined output threads\n");

    return 0;
}
