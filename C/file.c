#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include "tools.h"

int file_open(const char* url, int flags, mode_t mode)
{
    char filename[strlen(url) + 1];
    if(parse_url(url, NULL, NULL, NULL,  NULL, filename, NULL) == -1){
        fprintf(stderr, "failed to parse '%s' url\n", url);
        return -1;
    }
    int fd = open(filename, flags, mode);
    if(fd == -1)
        fprintf(stderr, "failed to open '%s' filename\n", filename);

    return fd;
}

int file_open_for_write(const char* url)
{
    return file_open(url, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP);
}

int file_open_for_read(const char* url)
{
    return file_open(url, O_RDONLY, S_IRUSR|S_IWUSR|S_IRGRP);
}
