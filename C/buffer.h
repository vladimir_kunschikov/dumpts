/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 'one producer - multiple consumers' sample.                                   *
 * source: File descriptor of the producer                                       *
 * destination*: File descriptors and allowed MPEG TS PID of the consumers       *
 * read_bitrate: producer read speed limit in Kb                                 *
 * write_bitrate: consumer write speed limit in Kb                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include "destination.h"
int read_and_write(int source, destination_t* dest, int dest_count, int read_bitrate, int write_bitrate);
