#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <poll.h>
#include <time.h>
#include "tools.h"
#include <netinet/in.h>
#include <arpa/inet.h>

void print_error(const char* action, const char* filename)
{
    char buffer[4096] = {0};
    if(filename)
        sprintf(buffer, "failed to %s '%s': ", action, filename);
    else
        sprintf(buffer, "failed to %s: ", action);
    strerror_r(errno, buffer + strlen(buffer), sizeof(buffer) - strlen(buffer) - 1);
    fwrite(buffer, 1, strlen(buffer), stderr);
}

int poll_and_read(int sd, int timeout, char* buffer, size_t size)
{
    int status = 0;
    struct pollfd pollfds[1];
    pollfds[0].fd = sd;
    pollfds[0].events = POLLIN;

    status = poll(pollfds, 1, timeout);
    if(status < 0){
        if(errno == EINTR)
            return 0;

        print_error("poll", "input descriptor");
        return status;
    }

    if(pollfds[0].revents != POLLIN)
        return 0;

    if((status = read(sd, buffer, size)) < 0)
        print_error("read", "input descriptor");
    if(!status){
        errno = EPIPE;
        status = -1;
        fprintf(stderr, "EOF deteted\n");
    }

    return status;
}

uint64_t uptime()
{
    struct timespec tv;

    clock_gettime(CLOCK_MONOTONIC, &tv);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_nsec / 1000;
}

int lock(pthread_mutex_t * mutex)
{
    return pthread_mutex_lock(mutex);
}

int unlock(pthread_mutex_t * mutex)
{
    return pthread_mutex_unlock(mutex);
}

int wait(pthread_cond_t *cond, pthread_mutex_t* mutex)
{
    return pthread_cond_wait((pthread_cond_t *)cond, (pthread_mutex_t*)mutex);
}

int wake(pthread_cond_t *cond)
{
    return pthread_cond_broadcast(cond);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * parse_url()                                                                   *
 * parses url to the host port filename triple                                   *
 * in: 'url' in http://example.org:888/a/b/c/d/file.jpg form                     *
 * out: 'host': example.org in the url just above this line                      *
 * out: 'page': page url part                                                    *
 * out: 'port': 80 if not specified                                              *
 * out: 'filename': file.jpg                                                     *
 * ret: 0 on success non zero on error                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int parse_url(const char* url, char* proto, char* host, uint16_t *port, char* page, char* filename,
        char* pids)
{
    //HOST
    const char* host_start = url;
    const char* prefix = strstr(url, "://");
#ifdef debug
    printf("%d: url: %s, prefix: %s\n", __LINE__, url, prefix);
#endif
    if(proto)
        proto[0] = 0;
    if(prefix){
        host_start = prefix + 3;
        int proto_len = prefix - url;
        if(proto){
            strncpy(proto, url, proto_len);
            proto[proto_len] = 0;
        }
    }
    const char* host_end = strchr(host_start, '/');
    if(!host_end){
        //        fprintf(stderr, "failed to find 'host/file' sequence in the '%s' url\n", url);
        host_end = host_start + strlen(host_start);
    }
    if(host){
        if(host_end != host_start)
            strncpy(host, host_start, host_end - host_start); 
        host[host_end - host_start] = 0;
    }

    //optional PORT
    const char* port_start = strchr(host_start, ':');
    if(port_start){
        if(host){
            strncpy(host, host_start, port_start - host_start);
            host[port_start - host_start] = 0;
        }
        if(port)
            *port = strtol(port_start + 1, NULL, 10);
        host_end = port_start;
    }else if(port)
        *port = 80;

    //optional PAGE
    if(page){
        const char* page_start = strchr(host_end, '/');
        if(page_start){
            const char* page_end = strchr(page_start, '?');
            if(!page_end)
                page_end = url + strlen(url);
            strncpy(page, page_start, page_end - page_start);
            page[page_end - page_start] = 0;
        }else
            strcpy(page, "/");
    }

    //optional FILENAME
    const char* filename_start = strrchr(url, '/');
    if(!prefix)
        filename_start = url - 1;
    else if(!strncasecmp("file", url, prefix - url))
        filename_start = prefix + 2;
    if(filename){
        filename[0] = 0;
        if(filename_start)
            strcpy(filename, filename_start + 1);
        else{
            //failed to find '/file' sequence
            strncpy(filename, host_start, host_end - host_start);
            filename[host_end - host_start] = 0;
        }
        char * filename_end = strchr(filename, '?');
        if(filename_end)
            *filename_end = 0;
    }
    //optional PID
    if(pids)
        *pids = 0;
    if(filename)
        filename_start = filename;
    else 
        filename++;
    char* pids_start = strchr(filename_start? filename_start : url, '{');
    if(!pids_start)
        return 0;
    if(filename)
        *pids_start = 0;
    pids_start++;
    char* pids_end = strchr(pids_start, '}');
    if(!pids_end)
        return 0;
    if(pids){
        strncpy(pids, pids_start, pids_end - pids_start);
        pids[pids_end - pids_start] = 0;
    }

    return 0;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * write_and_check()                                                             *
 * writes whole given buffer                                                     *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * in: 'dest' optional data destination for the send_to() transmition case       *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int write_and_check(int sd, const char* buffer, size_t size)
{
    ssize_t already_sent = 0;
    do{
        ssize_t block_size = write(sd, buffer + already_sent, size - already_sent);
        if(block_size == -1){
            fprintf(stderr, "failed to write: %s\n", strerror(errno));
            return -1;
        }
        already_sent += block_size;
    }while(already_sent < size);
    return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * send_to()                                                                     *
 * sendto() wrapper with actually transmitted size control                       *
 * in: 'data, size' outgoing data                                                *
 * in: 'host, port' destination                                                  *
 * ret: zero if no errors, -1 in other case                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int send_to(int sd, const char* data, size_t size, const char* host, uint16_t port)
{
    struct sockaddr_in addr;
    memset(&addr, 0,  sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_addr.s_addr=inet_addr(host);
    addr.sin_port=htons(port);

    size_t already_sent = 0;
    while(already_sent < size){
        ssize_t count = sendto(sd, data + already_sent, size - already_sent, 0, (struct sockaddr *)&addr, sizeof(addr));
        if(count < 0){
            fprintf(stderr, "Failed to send to the %s:%d!\n", host, port);
            return -1;
        }
        already_sent += count;
    }
    return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * retransmit()                                                                  *
 * wrapper over write_and_check() and sendto()                                   *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * in: 'dest' optional data destination for the send_to() transmition case       *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int retransmit(int sd, const char* data, size_t size, const destination_t* destination)
{
    if(!destination|| !destination->host)
        return write_and_check(sd, data, size);
    return send_to(sd, data, size, destination->host, destination->port);
}

