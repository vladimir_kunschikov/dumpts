/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * source stream initializer                                                     *
 * in: 'url' file://in.ts  udp://224.2.2.1:132 http://www.ru/t.ts etc            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
int init_source(const char* url);
