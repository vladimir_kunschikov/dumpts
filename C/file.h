/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * File read/write handlers                                                      *
 * in: 'url'  file://1.ts 1.ts aa.ts pat.ts{0} video.ts{256} EIT{17}             *
 * ret:  file descriptor or -1 on error                                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
int file_open_for_write(const char* url);
int file_open_for_read(const char* url);
