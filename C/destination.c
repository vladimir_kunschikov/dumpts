#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "destination.h"
#include "tools.h"
#include "http.h"
#include "file.h"
#include "udp.h"

int* parse_pids(const char* pids)
{
    if(!pids || !strlen(pids))
        return NULL;

    int count = 0;
    const char *comma = pids;

    for(count++; (comma = strchr(comma, ',')); count++, comma++);

    int* ret = malloc(sizeof(int) * (count + 1));

    for(count = 0, comma = pids; (comma = strchr(comma, ',')); count++, comma++)
        ret[count] = atoi(comma + 1);

    ret[count] = -1;
    return ret;
}


int init_destination(const char** urls, destination_t* destinations)
{
    int i = 0;
    for(i = 0; *urls; urls++){
        memset(destinations + i, 0, sizeof(destination_t));
        int fd = -1;
        const char* url = *urls;
        char proto[strlen(url) + 1];
        char host[strlen(url) + 1];
        char pid[strlen(url) + 1];
        uint16_t port;
        if(parse_url(url, proto, host, &port, NULL, NULL, pid) == -1)
            fprintf(stderr, "failed to parse '%s' url\n", url);
#ifdef debug
        printf("%s:%d url: %s, proto: %s, host: %s, pid: %s\n", __FILE__, __LINE__, url, proto, host, pid);s
#endif
        if(!strcasecmp(proto, "HTTP"))
            fd = http_output(url);
        else if(!strcasecmp(proto, "udp")){
            fd = open_multicast_socket(host, port);
            destinations[i].host = strdup(host); 
            destinations[i].port = port;
        }else //if(!strcasecmp(proto, "file"))
            fd = file_open_for_write(url);
        if(fd == -1)
            continue;

        destinations[i].fd = fd; 
        destinations[i++].pids = parse_pids(pid);
    }

    return i;
}
