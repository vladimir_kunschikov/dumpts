#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <netdb.h>
#include <errno.h>
#include <poll.h>
#include "tools.h"
#include "http.h"
#define CRLF "\r\n"

int verbosity = 0;
/* * * * * * * * * * * * *
 * function declarations *
 * * * * * * * * * * * * */
int http_status(int);
int relocate(int);
int http_common(const char* url, const char* query);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * handle_http_status()                                                          *
 * reads status from the newly created connection                                *
 * in: 'sd' socket descriptor                                                    *
 * ret: socket descriptor or -1                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int handle_http_status(int sd)
{
    switch(http_status(sd)){
        case 301:
        case 302:
        case 303:
            return relocate(sd);
        case 200:
        case 206:
            return sd;
    }
    fprintf(stderr, "Rejected to download.\n");
    close(sd);
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ipv4_from_string()                                                            *
 * inet_pton() wrapper                                                           *
 * in: 'hostname' in 1.2.3.4 notation                                            *
 * ret: int32_t IP in host order on success or 0 on error                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int ipv4_from_string(const char* hostname)
{
    struct sockaddr_in sa;
    // store this IP address in sa:
    if(1 == inet_pton(AF_INET, hostname, &(sa.sin_addr)))
        return ntohl(sa.sin_addr.s_addr);
    return 0;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ipv4_connect()                                                                *
 * simple connection to the ip given in ipv4 network form                        *
 * in: 'ip' IP address in the host order                                         *
 * in: 'port' PORT in the network order                                          *
 * ret: sd on success -1 on error                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int ipv4_connect(int ip, uint16_t port)
{
    int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sd == -1){
        fprintf(stderr, "Socket creation failed: %s\n", strerror(errno));
        return -1;
    }

    struct sockaddr_in sa;
    memset(&sa, 0, sizeof(struct sockaddr_in));
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = htonl(ip);
    sa.sin_port = htons(port);
    if(connect(sd, (struct sockaddr *)&sa, sizeof(struct sockaddr)) == -1){
        fprintf(stderr, "Connection failed: %s\n", strerror(errno));
        close(sd);
        return -1;
    }

    return sd;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_send_cmd()                                                               *
 * composes proper HTTP GET query and writes it to the given descriptor          *
 * in: sd connection descriptor                                                  *
 * in: url: url to be written                                                    *
 * in: host: host to be specified as http query attribute                        *
 * out: traffic to the sd                                                        *
 * ret: -1 on error 0 on success                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_send_cmd(int sd, const char* cmd, const char *url, const char* host)
{
    char query[strlen(url) + strlen(host) + 256];
    snprintf(query, sizeof(query), "%s %s HTTP/1.1" CRLF "Host: %s" CRLF CRLF, cmd, url, host);
    if(verbosity)
        printf(query);
    if(write_and_check(sd, query, strlen(query)) == -1){
        fprintf(stderr, "Failed to write to the %s: %s", url, strerror(errno));
        return -1;
    }
    return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * read_header()                                                                 *
 * reads HTTP header and parses the 'Content-Length parameter                    *
 * in:'sd' socket descriptor                                                     *
 * in:'tag' end  of header tag                                                   *
 * ret: allocated buffer, which needs to be freed by free()                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
char* read_header(int sd, const char* tag)
{
    if(verbosity)
        printf("started read of the header {%s}\n", tag);
    int header_allocation_size = 1024;
    char *buffer = malloc(header_allocation_size);
    if(!buffer){
        fprintf(stderr, "failed to allocate buffer of %d size\n", header_allocation_size);
        return NULL;
    }
    size_t size = 0;
    for(;;){
        ssize_t status = read(sd, buffer + size, 1);
        if(status < 1){
            fprintf(stderr, "failed to read header: %s\n", strerror(errno));
            break;
        }
        if(verbosity)
            printf("%c", buffer[size]);
        size++;
        if(size == header_allocation_size - 1){
            header_allocation_size *= 2;
            buffer = realloc(buffer, header_allocation_size);
            if(!buffer){
                fprintf(stderr, "failed to allocate buffer of %d size\n", header_allocation_size);
                return NULL;
            }
        }

        if(size >= strlen(tag) && !strncmp(buffer + size - strlen(tag), tag, strlen(tag)))
            break;
    }
    printf("Finished read of the header. Has read %lu bytes.\n", size);
    buffer[size] = 0;
    return buffer;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * parse_tag()                                                                   *
 * parses HTTP header and seatches specified tag                                 *
 * in:'header' input buffer                                                      *
 * out: value                                                                    *
 * ret: 1 on success, 0 on 'tag not found'                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int parse_tag(const char* header, const char* tag, char* value, size_t length)
{
    const char *line = header;
    size_t size = 0;
    value[size] = 0;
    for(;;){
        if(!line[size])
            return 0;

        if(size && line[size - 1] == '\r' && line[size] == '\n'){
            if(size == 1)
                break;

            if(size > strlen(tag) && !strncasecmp(line, tag, strlen(tag))){
                size_t real_value_len = size - strlen(tag) - 1;
                size_t val_len = real_value_len > length - 1? length - 1 : real_value_len;
                strncpy(value, line + strlen(tag), val_len);
                value[val_len] = 0;
                return 1;
            }
            line = line + size + 1;
            size = 0;
            continue;
        }
        size++;
    }

    return 0;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * tcp_advanced_connect() connects to any ip of the specified host:port address  *
 * in: hostname: connection target                                               *
 * in: port: destination port of the connection                                  *
 * ret: sd on success -1 on error                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int tcp_advanced_connect(const char* hostname, uint16_t port)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;

    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM; 
    hints.ai_flags = AI_PASSIVE;
    char port_string[8] = {0};
    sprintf(port_string, "%d", port);
    int s = getaddrinfo(hostname, port_string, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo(%s): %s\n", hostname, gai_strerror(s));
        return -1;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next){
        int sd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(sd == -1)
            continue;
        if(connect(sd, rp->ai_addr, rp->ai_addrlen) == -1){
            close(sd);
            continue;
        }

        freeaddrinfo(result);
        return sd;
    }

    freeaddrinfo(result);
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_status(sd)                                                               *
 * checks whether we are allowed to download this file                           *
 * reads first line and checks status                                            *
 * in: 'sd' newly created tcp connection to the HTTP server                      *
 * ret: HTTP reply code or -1                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_status(int sd)
{
    char line[1024] = {0};
    size_t size = 0;
    while(size < sizeof(line)){
        ssize_t status = read(sd, line + size, 1);
        if(status < 1)
            return 0;
        if(verbosity)
            printf("%c", line[size]);
        if(size && line[size - 1] == '\r' && line[size] == '\n'){
            char* space = strchr(line, ' ');
            if(!space){
                fprintf(stderr, "wrong server reply: %s\n", line);
                return 0;
            }
            return strtol(space + 1, NULL, 10);
        }
        size++;
    }
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * relocate()                                                                    *
 * checks presence of the 'location' field in the reply header                   *
 * in: socket                                                                    *
 * ret: new connection socket or -1                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int relocate(int sd)
{
    char* header = read_header(sd, CRLF CRLF);
    if(!header){
        fprintf(stderr, "failed to read HTTP header!\n");
        return -1;
    }

    char location[1024] = {0};
    int status = parse_tag(header, "location: ", location, sizeof(location));
    close(sd);
    free(header);

    if(verbosity)
        printf("Relocation. New url: %s\n", location);
    if(status == 1)
        return http_common(location, "GET");
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_common()                                                                 *
 * Establishes tcp connection, sends cmd, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * in: 'md'  GET or PUT or POST                                                  *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_common(const char* url, const char* query)
{
    uint16_t port; 
    char host[strlen(url) + 1], page[strlen(url) + 1];
    if(parse_url(url, NULL, host, &port, page, NULL, NULL) == -1){
        fprintf(stderr, "failed to parse '%s' url\n", url);
        return -1;
    }

    if(verbosity)
        printf("Trying to download %s.\n", url);

    int sd = -1;
    int ip = ipv4_from_string(host);
    if(ip)
        sd = ipv4_connect(ip, port);
    if(sd == -1)
        sd = tcp_advanced_connect(host, port);

    if(sd == -1){
        fprintf(stderr, "failed to establish HTTP connection to the '%s'\n", url);
        return -1;
    }

    if(http_send_cmd(sd, query, page, host) == -1){
        fprintf(stderr, "failed to send '%s' to the '%s'\n", query, url);
        close(sd);
        return -1;
    }

    return handle_http_status(sd);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_input()                                                                  *
 * Establishes tcp connection, sends GET, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_input(const char* url)
{
    int sd = http_common(url, "GET");
    char* s;
    if((s = read_header(sd, CRLF CRLF)))
        free(s);
    return sd;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_output()                                                                 *
 * Establishes tcp connection, sends PUT, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_output(const char* url)
{
    return http_common(url, "PUT");
}
