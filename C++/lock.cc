#include "lock.h"
#include "tools.h"
#include <iostream>
Lock::Lock()
{
    if(pthread_mutex_init(&mutex, NULL))
        std::cerr << "pthread_mutex_init() error: " << string_error() << std::endl;
}

Lock::~Lock()
{
    if(pthread_mutex_destroy(&mutex))
        std::cerr << "pthread_mutex_destroy() error: " << string_error() << std::endl;
}

void Lock::lock()
{
    if(pthread_mutex_lock(&mutex))
        std::cerr << "pthread_mutex_lock() error: " << string_error() << std::endl;
}

void Lock::unlock()
{
    if(pthread_mutex_unlock(&mutex))
        std::cerr << "pthread_mutex_unlock() error: " << string_error() << std::endl;
}

pthread_mutex_t* Lock::get_mutex_implementation()
{
    return &mutex;
}
