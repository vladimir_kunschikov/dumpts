/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Buffer class                                                                  *
 * Synchronized thread safe data storage with io bitrate limitation support      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <stdint.h>
#include  <string>
#include <vector>
#include <set>
#include "lock.h"
#include "wait_condition.h"
using std::string;
using std::vector;
using std::set;
class Buffer{
    public:
        Buffer(int input_bitrate = 0, int output_bitrate = 0, size_t max_buffer_size = 1024);
        ~Buffer();
        void set_input_bitrate_limit(int);
        void set_output_bitrate_limit(int);
        void set_max_buffer_size(size_t);
        size_t get_max_buffer_size();
        size_t get_start_position();
        bool write_data_to_buffer(const string& data); // producer adds 'data' to the buffer
        bool read_data_from_buffer(string& data, size_t& position); //consumer reads from buffer 'position' to 'data' 
    private:
        vector<string> buffer;
        vector<int> unit_thread_count;
        set<uint64_t> producers, consumers;
        int input_bitrate, output_bitrate; //bitrate limit for each of the producer/consumer
        size_t input_bitrate_counter, output_bitrate_counter; //temporary counters for the bitrate calculation
        uint64_t input_bitrate_timestamp, output_bitrate_timestamp;//temporary timestamps for the bitrate calculation
        size_t max_buffer_size; //max count of entries in the buffer
        size_t position; //current producers position in the buffer: buffer[position] is being written at the moment
        Lock data_lock; //thread synchronization primitive
        WaitCondition condition; //'unlock and sleep' condition for the 'data_lock'
        void limit_bitrate(const int bitrate, size_t& bitrate_block_count, uint64_t& bitrate_timestamp);
        bool update_write_position(size_t& thread_write_position); //consumer helper
        void update_current_slot(const string& data); //producer helper
        void wait_for_consumers(); //sleep if consumers are too slow for the producers
};
