#include "buffer.h"
#include "locker.h"
#include "tools.h"

Buffer::Buffer(int input_bitrate, int output_bitrate, size_t max_buffer_size):
    buffer(max_buffer_size),
    unit_thread_count(max_buffer_size),
    input_bitrate(input_bitrate),
    output_bitrate(output_bitrate), 
    input_bitrate_counter(0),
    output_bitrate_counter(0),
    input_bitrate_timestamp(0),
    output_bitrate_timestamp(0),
    max_buffer_size(max_buffer_size),
    position(0),
    condition(&data_lock)
{
}

Buffer::~Buffer()
{
    condition.wake();
}

void Buffer::set_input_bitrate_limit(int rate)
{
    input_bitrate = rate;
}

void Buffer::set_output_bitrate_limit(int rate)
{
    output_bitrate = rate;
}

void Buffer::set_max_buffer_size(size_t size)
{
    max_buffer_size = size;
}

size_t Buffer::get_max_buffer_size()
{
    return max_buffer_size;
}

size_t Buffer::get_start_position()
{
    return max_buffer_size;
}

void Buffer::update_current_slot(const string& data)
{
    size_t size = data.size();
    Locker locker(&data_lock);
    buffer[position].assign(data);
    input_bitrate_counter += size;

    uint64_t thread_id = gettid();
    if(not producers.count(thread_id))
        producers.insert(thread_id);

    if(data.empty())
        producers.erase(thread_id);
}

void Buffer::limit_bitrate(int bitrate, size_t& bitrate_block_count, uint64_t& bitrate_timestamp)
{
    if(!bitrate_timestamp){
        bitrate_timestamp = uptime();
        return;
    }

    if(bitrate <= 0)
        return;

    if(uptime() < bitrate_timestamp + 10)
        return;

    while(bitrate_block_count > bitrate * (uptime() - bitrate_timestamp)/1024)
        usleep(10);

    bitrate_block_count = 0;
    bitrate_timestamp = uptime();
}

void Buffer::wait_for_consumers()
{
    Locker locker(&data_lock);
    position = position + 1 < max_buffer_size? position + 1 : 0;
    
    while(unit_thread_count[position])
        condition.wait();

    condition.wake();
}

bool Buffer::write_data_to_buffer(const string& data)
{
    update_current_slot(data);
    limit_bitrate(input_bitrate, input_bitrate_counter, input_bitrate_timestamp);
    wait_for_consumers();

    return true;
}

bool Buffer::update_write_position(size_t& thread_write_position)
{
    Locker locker(&data_lock);

    if(thread_write_position != max_buffer_size){
        unit_thread_count[thread_write_position]--;
        thread_write_position++;
    }

    if(thread_write_position == max_buffer_size)
        thread_write_position = 0;

    if(position == thread_write_position and producers.empty())
        condition.wait(1);

    while(position == thread_write_position and not producers.empty())
        condition.wait(1);

    if(position == thread_write_position and producers.empty())
        return false;

    unit_thread_count[thread_write_position]++;
    return true;
}

bool Buffer::read_data_from_buffer(string& data, size_t& thread_position)
{
    if(not update_write_position(thread_position))
        return false;

    limit_bitrate(output_bitrate, output_bitrate_counter, output_bitrate_timestamp);
    data = buffer[thread_position];
    output_bitrate_counter += data.size();

    return true;
}
