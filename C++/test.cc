#include <cppunit/TestCase.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include "tools.h"
class UrlParserTest: public CppUnit::TestCase{
    public:
        void test_url1()
        {
            map<string, string> tokens;
            string url = "http://www.ru";
            parse_url(url, tokens);
            CPPUNIT_ASSERT(tokens["type"] ==  "http");
            CPPUNIT_ASSERT(tokens["host"] ==  "www.ru");
            tokens.clear();

            url = "http://www.ru/1.jpg";
            parse_url(url, tokens);
            CPPUNIT_ASSERT(tokens["type"] == "http");
            CPPUNIT_ASSERT(tokens["host"] == "www.ru");
            CPPUNIT_ASSERT(tokens["file"] == "1.jpg");
        }

        void test_url2()
        {
            map<string, string> tokens;
            string url = "1.txt";
            parse_url(url, tokens);
            CPPUNIT_ASSERT(tokens["file"] == "1.txt");
            tokens.clear();

            url = "file://2.txt";
            parse_url(url, tokens);
            CPPUNIT_ASSERT(tokens["type"] == "file");
            CPPUNIT_ASSERT(tokens["file"] == "2.txt");
        }
        void test_url3()
        {
            map<string, string> tokens;
            parse_url("udp://128.1.1.1:abcd", tokens);
            CPPUNIT_ASSERT(tokens["type"] == "udp");
            CPPUNIT_ASSERT(tokens["host"] == "128.1.1.1");
            CPPUNIT_ASSERT(tokens["port"] == "");
        }

        void test_url4()
        {
            map<string, string> tokens;
            parse_url("udp://128.1.1.1:1234", tokens);
            CPPUNIT_ASSERT(tokens["type"] == "udp");
            CPPUNIT_ASSERT(tokens["host"] == "128.1.1.1");
            CPPUNIT_ASSERT(tokens["port"] == "1234");
        }

        void test_url5()
        {
            map<string, string> tokens;
            parse_url("http://host.ru:7777/main/file.jpg?param=value&param2=value2", tokens);
            CPPUNIT_ASSERT(tokens["type"] == "http");
            CPPUNIT_ASSERT(tokens["host"] == "host.ru");
            CPPUNIT_ASSERT(tokens["port"] == "7777");
            CPPUNIT_ASSERT(tokens["file"] == "file.jpg");
            CPPUNIT_ASSERT(tokens["page"] == "/main/file.jpg");
        }

        void test_url6()
        {
            map<string, string> tokens;
            parse_url("http://host.ru:7777/file.jpg{12,13,14}?param=value&param2=value2", tokens);
            CPPUNIT_ASSERT(tokens["type"] == "http");
            CPPUNIT_ASSERT(tokens["host"] == "host.ru");
            CPPUNIT_ASSERT(tokens["port"] == "7777");
            CPPUNIT_ASSERT(tokens["page"] == "/file.jpg{12,13,14}");
            CPPUNIT_ASSERT(tokens["file"] == "file.jpg");
            CPPUNIT_ASSERT(tokens["pid"] == "12,13,14");
        }

        void test_url7()
        {
            map<string, string> tokens;
            parse_url("../file.txt", tokens);
            CPPUNIT_ASSERT(tokens["file"] == "../file.txt");
        }

        void test_url8()
        {
            map<string, string> tokens;
            parse_url("../../file.txt{3,2,1}", tokens);
            CPPUNIT_ASSERT(tokens["file"] == "../../file.txt");
            CPPUNIT_ASSERT(tokens["pid"] == "3,2,1");
        }

        void test_url9()
        {
            map<string, string> tokens;
            parse_url("http://localhost/file.txt", tokens);
            CPPUNIT_ASSERT(tokens["file"] == "file.txt");
            CPPUNIT_ASSERT(tokens["host"] == "localhost");
        }

        void test_url10()
        {
            map<string, string> tokens;
            parse_url("http://localhost/file.txt{3,2,1}", tokens);
            CPPUNIT_ASSERT(tokens["file"] == "file.txt");
            CPPUNIT_ASSERT(tokens["pid"] == "3,2,1");
            CPPUNIT_ASSERT(tokens["host"] == "localhost");
        }

        void test_url11()
        {
            map<string, string> tokens;
            parse_url("file{3,2,1}", tokens);
            CPPUNIT_ASSERT(tokens["file"] == "file");
            CPPUNIT_ASSERT(tokens["pid"] == "3,2,1");
        }

        void test_url12()
        {
            map<string, string> tokens;
            parse_url("file", tokens);
            CPPUNIT_ASSERT(tokens["file"] == "file");
        }

        CPPUNIT_TEST_SUITE(UrlParserTest);
        CPPUNIT_TEST(test_url1);
        CPPUNIT_TEST(test_url2);
        CPPUNIT_TEST(test_url3);
        CPPUNIT_TEST(test_url4);
        CPPUNIT_TEST(test_url5);
        CPPUNIT_TEST(test_url6);
        CPPUNIT_TEST(test_url7);
        CPPUNIT_TEST(test_url8);
        CPPUNIT_TEST(test_url9);
        CPPUNIT_TEST(test_url10);
        CPPUNIT_TEST(test_url11);
        CPPUNIT_TEST(test_url12);
        CPPUNIT_TEST_SUITE_END();
};

        CPPUNIT_TEST_SUITE_REGISTRATION(UrlParserTest);
int main(int argc, char ** argv)
{
    CppUnit::Test *test = CppUnit::TestFactoryRegistry::getRegistry().makeTest();
    CppUnit::TextTestRunner runner;
    runner.addTest(test);
    runner.run();
    return 0;
}
