/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MPEG2 TS toolkit. pids, payloads, CC control, pats, pmts, sdts and so on      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#define TS_PACKET_SIZE          188
#include <stdint.h>
uint16_t get_pid(const char* ts_packet);
