#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <poll.h>
#include <time.h>
#include "tools.h"
#include <sys/syscall.h>   /* For SYS_xxx definitions */
#include <iostream>
int parse_url(const string& url, map<string, string>& tokens)
{
    //TYPE
    size_t prefix_pos = url.find("://");
    if(prefix_pos != string::npos)
        tokens["type"] = url.substr(0, prefix_pos);
    //HOST
    size_t host_start = 0;
    if(prefix_pos != string::npos)
        host_start = prefix_pos + 3;

    size_t host_end = url.find("/", host_start);
    tokens["host"] = url.substr(host_start, host_end - host_start);

    //PORT
    size_t port_start = url.find(":", host_start);
    if(port_start != string::npos){
        tokens["host"] = url.substr(host_start, port_start - host_start);
        port_start++;
        size_t port_end = url.find_first_not_of("1234567890", port_start);
        tokens["port"] = url.substr(port_start, port_end - port_start);
    }

    //optional PAGE
    if(host_end != string::npos){
        size_t page_start = url.find("/", host_end);
        if(page_start != string::npos){
            size_t page_end = url.find("?", page_start);
            tokens["page"] = url.substr(page_start, page_end - page_start);
        }
    }

    size_t filename_start = url.rfind("/");
    if(filename_start == string::npos)
        tokens["file"] = tokens["host"];
    else
        tokens["file"] = url.substr(filename_start + 1);

    if(tokens.count("type") and tokens["type"] == "file")
        tokens["file"] = url.substr(prefix_pos + 3);
    else if(!tokens.count("type"))
        tokens["file"] = url;

    size_t filename_end = tokens["file"].find("?");
    if(filename_end != string::npos)
        tokens["file"].erase(filename_end);
    string& filename = tokens["file"];
    size_t pids_start = filename.find("{");
    if(pids_start != string::npos){
        size_t pids_end = filename.find("}", pids_start);
        if(pids_end != string::npos){
            tokens["pid"] = filename.substr(pids_start + 1, pids_end - pids_start  - 1);
            filename.erase(pids_start, pids_end - pids_start + 1);
        }
    }
    return tokens.size();
}

std::string string_error()
{
    char message[1024] = {0};
    strerror_r(errno, message, sizeof(message));
    return message;
}

uint64_t uptime()
{
    struct timespec tv;

    clock_gettime(CLOCK_MONOTONIC, &tv);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_nsec / 1000;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * write_and_check()                                                             *
 * writes whole given buffer                                                     *
 * in: 'sd' file descriptor                                                      *
 * in: 'buffer' data to be written                                               *
 * in: 'size' size of the data to be written                                     *
 * ret: status of the operation: -1 on error 0 on success                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int write_and_check(int sd, const char* buffer, size_t size)
{
    size_t already_sent = 0;
    do{
        ssize_t block_size = write(sd, buffer + already_sent, size - already_sent);
        if(block_size == -1){
            fprintf(stderr, "failed to write: %s\n", strerror(errno));
            return -1;
        }
        already_sent += block_size;
    }while(already_sent < size);
    return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * poll_for_input()                                                              *
 * polls given descriptor for incoming data                                      *
 * in: 'sd' file descriptor handle to be polled                                  *
 * in: 'timeout' timeout of data arrival                                         *
 * ret: true if any data is available, false on any error or timeout             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
bool poll_for_input(int sd, int timeout)
{
    int status = 0;
    struct pollfd pollfds[1];
    pollfds[0].fd = sd;
    pollfds[0].events = POLLIN;

    status = poll(pollfds, 1, 1000 * timeout);
    if(status < 0){
        if(errno == EINTR)
            return false;

        std::cerr << "failed to poll: " <<  string_error() << std::endl;
        return false;
    }

    if(pollfds[0].revents != POLLIN)
        return false;
    return status;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * gettid                                                                        *
 *  thread LWP identification                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
uint64_t gettid()
{
    return syscall(SYS_gettid);
}
