#include "stream_handler.h"

const int StreamHandler::OK = 0;
const int StreamHandler::Error = -1;
const int StreamHandler::NotInited = -2;

StreamHandler::StreamHandler():buffer(NULL){}

void StreamHandler::set_buffer(Buffer* b)
{
    buffer = b;
}
