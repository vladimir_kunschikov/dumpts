/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Locker class                                                                  *
 * simpliest possible pthread mutex lock/unlock wrapper                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
class Locker{
    class Lock* lock;
    public:
        Locker(Lock* lock);
        ~Locker();
};
