#pragma once
#include <string>
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_output()                                                                 *
 * Establishes tcp connection, sends PUT, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_output(const std::string& url);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_input()                                                                  *
 * Establishes tcp connection, sends GET, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_input(const std::string& url);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * poll_for_input()                                                              *
 * polls given descriptor for incoming data                                      *
 * in: 'sd' file descriptor handle to be polled                                  *
 * in: 'timeout' timeout of data arrival                                         *
 * ret: true if any data is available, false on any error or timeout             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
bool poll_for_input(int sd, int timeout);
