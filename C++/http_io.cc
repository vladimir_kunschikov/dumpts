#include "http_io.h"
#include "http_tools.h"
#include "mpegts.h"
#include "buffer.h"
#include "tools.h"

#include <stdlib.h>
#include <iostream>
using namespace std;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPCommon                                                                    *
 * base class for the HTTP IO stream handlers                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
HTTPCommon::HTTPCommon():sd(-1), state(StreamHandler::OK)
{}

HTTPCommon::~HTTPCommon()
{
    if(sd != -1)
        close(sd);
}

void HTTPCommon::set_url(const string& url)
{
    this->url = url;
}

int HTTPCommon::status() const
{
    return state;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPInput                                                                     *
 * HTTP Input 'GET' stream handler                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void HTTPInput::run()
{
    sd = http_input(url);
    if(sd == -1){
        std::cerr << "Refused to handle '" << url << "'[" << string_error() << "]" << std::endl;
        state = StreamHandler::Error;
        return;
    }
 
    size_t file_length = 0;
    while(poll_for_input(sd, 5)){
        char stream_buffer[TS_PACKET_SIZE];
        int status = read(sd, stream_buffer, sizeof(stream_buffer));
        if(status == -1){
            cerr << "failed to read " << url << ": " << string_error() << endl;
            break;
        }
        if(!status){
            cerr << "EOF while read from " << url << endl;
            break;
        }
 
        buffer->write_data_to_buffer(string(stream_buffer, status));
        file_length += status;
    }
    std::cout << "Has downloaded " << file_length << " bytes from the '" << url << "'." << std::endl;
    buffer->write_data_to_buffer(string());
}

StreamHandler* HTTPInput::clone()
{
    return new HTTPInput;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * HTTPOutput                                                                    *
 * HTTP Output 'Put' stream handler with MPEG TS packet filtering support        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void HTTPOutput::set_allowed_pid(const string& pids)
{
    const string digits = "0123456789";
    for(size_t pos = 0; pos != string::npos; pos = pids.find_first_of(digits, pos)){
        size_t start = pos;
        size_t end = pids.find_first_not_of(digits, pos);
        pos = end;
        allowed_pids.insert(atoi(pids.substr(start, end).c_str()));
    }
}

void HTTPOutput::run()
{
    map<string, string> tokens;
    parse_url(url, tokens);
    set_allowed_pid(tokens["pid"]);

    sd = http_output(url);
    if(sd == -1){
        std::cerr << "Refused to handle '" << url << "'[" << string_error() << "]" << std::endl;
        state = StreamHandler::Error;
        return;
    }
 
    size_t buffer_position = buffer->get_start_position();
    string data;

    while(buffer->read_data_from_buffer(data, buffer_position))
        if(allowed_pids.empty())
            write_and_check(sd, data.data(), data.size());
        else
            filter(data);
}

void HTTPOutput::filter(const string& data)
{
    for(size_t i = 0; i < data.size(); i += TS_PACKET_SIZE){
        if(data.size() - i < TS_PACKET_SIZE){
            write_and_check(sd, data.data() + i, data.size() - i);
            return;
        }

        const char* ts_packet = data.data() + i;
        if(ts_packet[0] == 0x47 and not allowed_pids.count(get_pid(ts_packet)))
            continue;

        write_and_check(sd, ts_packet, TS_PACKET_SIZE);
    }
}

StreamHandler* HTTPOutput::clone()
{
    return new HTTPOutput;
}
