/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Thread class                                                                  *
 * pthread_create() wrapper                                                      *
 * Derived classes must override run() method                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <pthread.h>
class Thread{
    public:
        Thread();
        virtual ~Thread();
        void start();
        void join();
        virtual void run() = 0;
    private:
        pthread_t thread;
        bool started;

};
