#include "locker.h"
#include "lock.h"
#include <iostream>

Locker::Locker(Lock* l):lock(l)
{
    if(lock)
        lock->lock();
    else
        std::cerr << "Wrong locker initialization: no lock" << std::endl;
}

Locker::~Locker()
{
    if(lock)
        lock->unlock();
    else
        std::cerr << "Wrong locker initialization: no lock" << std::endl;
}
