#include "stream_handler_factory.h"
#include "file_io.h"
#include "http_io.h"
#include "udp_io.h"
#include "buffer.h"
#include "pipe.h"
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

void print_usage(const std::string& appname)
{
    std::cout << "Multicast stream dumper/streamer with MPEG TS PID packet filtering support.\n"
        "Usage:\n"
        "\t " << appname << " SOURCE [OUTPUT] [OUTPUT] [OUTPUT] ...\n"
        "Parameters:\n"
        "\tSOURCE mandatory multicast input stream specification in host:port form."
        " Default port is 1234.\n"
        "\t\t Source multicast stream specification example with both host and port specified:\n"
        "\t\t\t 224.1.3.4:1235\n"
        "\t\t Source with udp:// prefix:\n"
        "\t\t\t udp://224.2.3.4:1234\n"
        "\t\t Source with default port 1234 fallback:\n"
        "\t\t\t 224.1.2.3\n"
        "\t [OUTPUT]"
        " optional output file path with optional MPEG TS PID specification in path{PIDs} form. Default: 'dump.ts'.\n"
        "\t\t Output file specification sample which dumps whole multicast stream to the /media/dump.ts:\n"
        "\t\t\t /media/stream.ts\n"
        "\t\t Output which dumps all 0 or 13 MPEG TS PID packets to the /media/pat_sdt.ts:\n"
        "\t\t\t /media/pat_sdt.ts{0,13}\n";
}


int main(int argc, char **argv)
{
    int opt, read_bitrate = 0, write_bitrate = 0;
    while((opt = getopt(argc, argv, "i:o:")) != -1){
        switch (opt) {
            case 'i':
                read_bitrate = atoi(optarg);
                break;
            case 'o':
                write_bitrate = atoi(optarg);
                break;
            default:
                print_usage(argv[0]);
                return -1;
        }
    }

    if(optind >= argc){
        print_usage(argv[0]);
        return 0;
    }

    StreamHandlerFactory factory;
    factory.register_input("file", new FileInput);
    factory.register_input("http", new HTTPInput);
    factory.register_input("udp", new MulticastInput);
    factory.register_output("file", new FileOutput);
    factory.register_output("http", new HTTPOutput);
    factory.register_output("udp", new MulticastOutput);

    const char* source_url = argv[optind];
    Pipe pipe;
    pipe.set_buffer(new Buffer(read_bitrate, write_bitrate));
    pipe.add_producer(factory.create_input(source_url));

    if(optind >= argc){
        std::cout << "No output destination were specified. Fallback to the default 'dump.ts'." << std::endl;
        pipe.add_consumer(factory.create_output("dump.ts"));
    }

    for(int i = optind + 1; i < argc; i++)
        pipe.add_consumer(factory.create_output(argv[i]));

    return pipe.run();
}
