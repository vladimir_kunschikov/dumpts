/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Pipe class                                                                    *
 * Basic 'multiple producers - multiple consumers' example.                      *
 * run(): main method. Starts producers, consumers; joins producers, consumers.  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <vector>
#include <string>
class StreamHandler;
class Buffer;
using std::vector;
class Pipe{
    public:
        ~Pipe();
        void set_buffer(Buffer*);
        void add_producer(StreamHandler*);
        void add_consumer(StreamHandler*);
        int run();
    private:
        Buffer* buffer;
        vector<StreamHandler*> consumers;
        vector<StreamHandler*> producers;
        size_t initialized(const vector<StreamHandler*>& handlers);
        void start(const vector<StreamHandler*>& handlers);
        void join(const vector<StreamHandler*>& handlers);
};
