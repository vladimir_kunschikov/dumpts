#pragma once
#include <string>
#include <stdint.h>
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * open_input_multicast_socket()                                                 *
 * create udp socket, joines group, binds to the reception address               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int open_input_multicast_socket(const std::string& host, uint16_t port);
