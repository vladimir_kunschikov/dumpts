#include "wait_condition.h"
#include "tools.h"
#include <time.h>
#include <iostream>
#include <errno.h>

WaitCondition::WaitCondition(Lock* lock):lock(lock)
{
    if(pthread_cond_init(&condition, NULL))
        std::cerr << "pthread_cond_init() error: " << string_error() << std::endl;
}

WaitCondition::~WaitCondition()
{
    if(pthread_cond_destroy(&condition))
        std::cerr << "pthread_cond_destroy() error: " << string_error() << std::endl;
}
    
void WaitCondition::wait(int seconds)
{
    if(seconds){
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += seconds;
        int rc = pthread_cond_timedwait(&condition, lock->get_mutex_implementation(), &ts);
        if(rc and rc != ETIMEDOUT)
            std::cerr << "pthread_cond_wait() error: " << string_error() << std::endl;
        return;
    }

    if(pthread_cond_wait(&condition, lock->get_mutex_implementation()))
        std::cerr << "pthread_cond_wait() error: " << string_error() << std::endl;
}

void WaitCondition::wake()
{
    if(pthread_cond_broadcast(&condition))
        std::cerr << "pthread_cond_broadcast() error: " << string_error() << std::endl;
}
