#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>

#include "udp_tools.h"
#include "tools.h"
#include  <iostream>
using std::cerr;
using std::endl;
int open_input_multicast_socket(const string& address, uint16_t port)
{
    int sd = -1;
    struct ip_mreq mreq;

    /* create what looks like an ordinary UDP socket */
    if ((sd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        cerr << "Failed to create UDP datagram socket! " << string_error() << endl;
        return sd;
    }

    int yes = 1;            
    /* allow multiple sockets to use the same PORT number */
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0)
        cerr << "Failed to set 'address reuse mode' to the UDP datagram socket. " << string_error() << endl;

    /* use setsockopt() to request that the kernel join a multicast group */
    mreq.imr_multiaddr.s_addr = inet_addr(address.c_str());
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
        cerr << "Failed to join UDP multicast group. " << string_error() << endl;
        close(sd);
        return -1;
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = mreq.imr_multiaddr.s_addr;
    addr.sin_port = htons(port);

    if (bind(sd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        cerr << "Failed to bind UDP multicast socket. " << string_error() << endl;
        close(sd);
        return -1;
    }

    return sd;
}
