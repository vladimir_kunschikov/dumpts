/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * pthread_cont_t wrapper                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <pthread.h>
#include "lock.h"
class WaitCondition{
    Lock* lock;
    pthread_cond_t condition;
    public:
    WaitCondition(Lock* lock);
    ~WaitCondition();
    void wait(int seconds = 0);
    void wake();
};
