#include "thread.h"
#include "tools.h"
#include  <iostream>

Thread::Thread():started(false){}
Thread::~Thread()
{
    if(started)
        join();
}

static void* start_routine(void *v)
{
    Thread* thread = reinterpret_cast<Thread*> (v);
    thread->run();
    return NULL;
}

void Thread::start()
{
    if(pthread_create(&thread, NULL, start_routine, this))
        std::cerr << "pthread_create() error: " << string_error() << std::endl;
    else
        started = true;
}

void Thread::join()
{
    if(not started){
        std::cerr << "thread wasn't started at all" << std::endl;
        return;
    }
    if(pthread_join(thread, NULL))
        std::cerr << "pthread_join() error: " << string_error() << std::endl;
    started = false;
}
