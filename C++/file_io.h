#pragma once
#include "stream_handler.h"
#include <fstream>
#include <set>
using std::string;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * FileIO                                                                        *
 * Base class for the file read write stream handlers                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class FileIO:public StreamHandler{
        int status() const;
    protected:
        FileIO();
        string url;
        string filename;
        int state;
        void set_url(const string& url);
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * FileInput                                                                     *
 * Input file stream handler                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class FileInput:public FileIO{
    std::ifstream f;
    StreamHandler* clone();
    void set_url(const string& url);
    void run();
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * FileOutput                                                                    *
 * File write stream handler with optional mpeg ts filtering                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class FileOutput:public FileIO{
    std::ofstream f;
    StreamHandler* clone();
    void set_url(const string& url);
    void run();
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MPEGTS PID filtering                                                          *
 * allowed_pids: set of the allowed to write pids : {0, 17, 256, etc}            *
 * All pids are allowed if allowed pids set is empty                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    std::set<int> allowed_pids;
    void set_allowed_pid(const string& pids);
    void filter(const string& data);
};
