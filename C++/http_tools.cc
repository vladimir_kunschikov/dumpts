#include "http_tools.h"
#include "tools.h"
#include "http_tools.h"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <netdb.h>
#include <errno.h>
#include <poll.h>

#include <iostream>
#include <sstream>
using namespace std;

#define CRLF "\r\n"
#ifdef debug
int verbosity = 1;
#else
int verbosity = 0;
#endif

/* * * * * * * * * * * * *
 * forward declarations  *
 * * * * * * * * * * * * */
int http_status(int);
int relocate(int);
int http_common(const string& url, const string& query);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * handle_http_status()                                                          *
 * reads status from the newly created connection                                *
 * in: 'sd' socket descriptor                                                    *
 * ret: socket descriptor or -1                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int handle_http_status(int sd)
{
    switch(http_status(sd)){
        case 301:
        case 302:
        case 303:
            return relocate(sd);
        case 200:
        case 206:
            return sd;
    }
    cerr << "Rejected to download.\n";
    close(sd);
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ipv4_from_string()                                                            *
 * inet_pton() wrapper                                                           *
 * in: 'hostname' in 1.2.3.4 notation                                            *
 * ret: int32_t IP in host order on success or 0 on error                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int ipv4_from_string(const string& hostname)
{
    struct sockaddr_in sa;
    // store this IP address in sa:
    if(1 == inet_pton(AF_INET, hostname.c_str(), &(sa.sin_addr)))
        return ntohl(sa.sin_addr.s_addr);
    return 0;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ipv4_connect()                                                                *
 * simple connection to the ip given in ipv4 network form                        *
 * in: 'ip' IP address in the host order                                         *
 * in: 'port' PORT in the network order                                          *
 * ret: sd on success -1 on error                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int ipv4_connect(int ip, uint16_t port)
{
    int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sd == -1){
        cerr << "Socket creation failed: " << string_error() << endl;
        return -1;
    }

    struct sockaddr_in sa;
    memset(&sa, 0, sizeof(struct sockaddr_in));
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = htonl(ip);
    sa.sin_port = htons(port);
    if(connect(sd, (struct sockaddr *)&sa, sizeof(struct sockaddr)) == -1){
        cerr << "Connection failed: " << string_error() << endl;
        close(sd);
        return -1;
    }

    return sd;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_send_cmd()                                                               *
 * composes proper HTTP GET query and writes it to the given descriptor          *
 * in: sd connection descriptor                                                  *
 * in: url: url to be written                                                    *
 * in: host: host to be specified as http query attribute                        *
 * out: traffic to the sd                                                        *
 * ret: -1 on error 0 on success                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_send_cmd(int sd, const string& cmd, const string& url, const string& host)
{
    stringstream ss;
    ss << cmd << " " << url << " HTTP/1.1" << CRLF << "Host: " << host << CRLF << CRLF;
    if(verbosity)
        cout << ss.str() << endl;
    if(write_and_check(sd, ss.str().data(), ss.str().size()) == -1){
        cerr << "Failed to write to the " << url << ": " << string_error() << endl;
        return -1;
    }
    return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * read_header()                                                                 *
 * reads HTTP header and parses the 'Content-Length parameter                    *
 * in:'sd' socket descriptor                                                     *
 * in:'tag' end  of header tag                                                   *
 * ret: allocated buffer, which needs to be freed by free()                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
string read_header(int sd, const string& tag)
{
    if(verbosity)
        cout << "started read of the header which should be ended by '" << tag << "'" << endl;
    string buffer;
    for(;;){
        char c;
        ssize_t status = read(sd, &c, 1);
        if(status < 1){
            fprintf(stderr, "failed to read header: %s\n", strerror(errno));
            break;
        }
        if(verbosity)
            cout << c;
        buffer.append(&c, 1);

        if(buffer.size() >= tag.size() && buffer.substr(buffer.size() - tag.size()) == tag)
            break;
    }
    if(verbosity)
        cout << "Finished read of the header. Has read " << buffer.size() << " bytes.\n";
    return buffer;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * parse_tag()                                                                   *
 * parses HTTP header and seatches specified tag                                 *
 * in:'header' input buffer                                                      *
 * out: value                                                                    *
 * ret: 1 on success, 0 on 'tag not found'                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
bool parse_tag(const string& header, const string& tag, string& value)
{
    string token = CRLF + tag;
    size_t pos = header.find(token);
    if(pos == string::npos)
        return false;
    size_t start = pos + token.size();
    size_t end = header.find(CRLF, start);
    value = header.substr(start, end - start);
    return true;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * tcp_advanced_connect() connects to any ip of the specified host:port address  *
 * in: hostname: connection target                                               *
 * in: port: destination port of the connection                                  *
 * ret: sd on success -1 on error                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int tcp_advanced_connect(const string& hostname, uint16_t port)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;

    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM; 
    hints.ai_flags = AI_PASSIVE;
    char port_string[8] = {0};
    sprintf(port_string, "%d", port);
    int s = getaddrinfo(hostname.c_str(), port_string, &hints, &result);
    if (s != 0) {
        cerr << "error in getaddrinfo(" << hostname << "):" << gai_strerror(s) << endl;
        return -1;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next){
        int sd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(sd == -1)
            continue;
        if(connect(sd, rp->ai_addr, rp->ai_addrlen) == -1){
            close(sd);
            continue;
        }

        freeaddrinfo(result);
        return sd;
    }

    freeaddrinfo(result);
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_status(sd)                                                               *
 * checks whether we are allowed to download this file                           *
 * reads first line and checks status                                            *
 * in: 'sd' newly created tcp connection to the HTTP server                      *
 * ret: HTTP reply code or -1                                                    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_status(int sd)
{
    string line;
    while(true){
        char c;
        ssize_t status = read(sd, &c, 1);
        if(status < 1)
            return 0;
        if(verbosity)
            printf("%c", c);
        line.append(&c, 1);
        if(line.size() > 2 and line.substr(line.size() - 2) == CRLF){
            size_t space = line.find(" ");
            if(space == string::npos){
                cerr << "wrong server reply: '" <<  line << "'" << endl;
                return 0;
            }
            return strtol(line.substr(space).c_str(), NULL, 10);
        }
    }

    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * relocate()                                                                    *
 * checks presence of the 'location' field in the reply header                   *
 * in: socket                                                                    *
 * ret: new connection socket or -1                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int relocate(int sd)
{
    string header = read_header(sd, CRLF CRLF);
    if(header.empty()){
        cerr << "failed to read HTTP header!\n";
        return -1;
    }

    string location;
    bool status = parse_tag(header, "location: ", location);
    close(sd);

    if(verbosity)
         cout << "Relocation. New url: '" << location << "'\n";

    if(status)
        return http_common(location, "GET");
    return -1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_common()                                                                 *
 * Establishes tcp connection, sends cmd, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * in: 'md'  GET or PUT or POST                                                  *
 * ret: zero on success -1 on error                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_common(const string& url, const string& query)
{
    map<string, string> tokens;
    parse_url(url, tokens);

    if(verbosity){
        cout << "Trying to download '" << url<< "': " << endl;
        for(map<string, string>::iterator i = tokens.begin(); i != tokens.end(); i++)
            cout << i->first << " -> " << i->second << endl;
    }
    const string& host = tokens["host"];
    if(host.empty()){
        cerr << "failed to get hostname from the url '" << url << "', rejected to handle." << endl;
        return -1;
    }
    uint16_t port = atoi(tokens["port"].c_str());
    if(not port)
        port = 80;
    int sd = -1;
    int ip = ipv4_from_string(host);
    if(ip)
        sd = ipv4_connect(ip, port);
    if(sd == -1)
        sd = tcp_advanced_connect(host, port);

    if(sd == -1){
        cerr << "failed to establish HTTP connection to the '" << url << "'" << endl;
        return -1;
    }

    if(http_send_cmd(sd, query, tokens["page"], host) == -1){
        cerr << "failed to send '" << query << "' to the '" << url << "'" << endl;
        close(sd);
        return -1;
    }

    return handle_http_status(sd);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_input()                                                                  *
 * Establishes tcp connection, sends GET, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_input(const string& url)
{
    int sd = http_common(url, "GET");
    if(sd != -1)
        read_header(sd, CRLF CRLF);
    return sd;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * http_output()                                                                 *
 * Establishes tcp connection, sends PUT, analizes server reply                  *
 * in: 'url' source data url                                                     *
 * ret: file descriptor on success -1 on error                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int http_output(const string& url)
{
    return http_common(url, "PUT");
}
