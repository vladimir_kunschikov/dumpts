/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Lock class                                                                    *
 * simpliest possible pthread mutex wrapper                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include <pthread.h>
class Lock{
    pthread_mutex_t mutex;
    public:
        Lock();
        ~Lock();
        void lock();
        void unlock();
        pthread_mutex_t* get_mutex_implementation();
};
