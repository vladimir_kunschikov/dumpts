#pragma once
#include "stream_handler.h"
#include <stdint.h>
#include <set>
using std::string;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * UDPCommon class                                                               *
 * base class for the UDP Multicast IO stream handlers                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class UDPCommon: public StreamHandler{
    public:
        UDPCommon();
        ~UDPCommon();
        void set_url(const string& url);
        int status() const;
    protected:
        int sd;
        uint16_t port;
        string host;
        string url;
        int state;
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MulticastInput class                                                          *
 * UDP Multicast Input Stream handler                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class MulticastInput: public UDPCommon{
    StreamHandler* clone();
    void run();
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MulticastOutput class                                                         *
 * UDP multicast streaming with MPEG TS packet filtering support                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class MulticastOutput: public UDPCommon{
    StreamHandler* clone();
    void run();
    void send(const char* data, size_t size);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MPEGTS PID filtering                                                          *
 * allowed_pids: set of the allowed to write pids : {0, 17, 256, etc}            *
 * All pids are allowed if allowed pids set is empty                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    std::set<int> allowed_pids;
    void set_allowed_pid(const string& pids);
    void filter(const string& data);
};
